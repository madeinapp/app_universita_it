<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

//Route::get('/phpinfo', function(){ phpinfo(); });

Auth::routes();

/** Reports */
Route::group([ 'prefix' => 'reports', 'as' => 'reports.' , 'middleware' => ['auth']] , function() {    

    Route::get('/{professional_family}/andamento', ['as' => 'andamento', 'uses' => 'ReportController@andamento']);
    Route::get('/{professional_family}/professioni', ['as' => 'professioni', 'uses' => 'ReportController@professioni']);
    Route::get('/{professional_family}/aziende', ['as' => 'aziende', 'uses' => 'ReportController@aziende']);
    Route::get('/{professional_family}/esperienza', ['as' => 'esperienza', 'uses' => 'ReportController@esperienza']);
    Route::get('/{professional_family}/contratto', ['as' => 'contratto', 'uses' => 'ReportController@contratto']);
    Route::get('/{professional_family}/bilancio_competenze', ['as' => 'bilancio_competenze', 'uses' => 'ReportController@bilancio_competenze']);
    Route::get('/{professional_family}/top_competenze', ['as' => 'top_competenze', 'uses' => 'ReportController@top_competenze']);
    Route::get('/{professional_family}/regioni', ['as' => 'regioni', 'uses' => 'ReportController@regioni']);
    Route::get('/{professional_family}/geo_annunci_prov', ['as' => 'geo_annunci_prov', 'uses' => 'ReportController@geo_annunci_prov']);
    Route::get('/{professional_family}/geo_vdm_reg', ['as' => 'geo_vdm_reg', 'uses' => 'ReportController@geo_vdm_reg']);
    Route::get('/{professional_family}/geo_vdm_prov', ['as' => 'geo_vdm_prov', 'uses' => 'ReportController@geo_vdm_prov']);

    Route::get('/{professional_family}/professione_esco', ['as' => 'professione_esco', 'uses' => 'ReportController@professioneESCO']);
    Route::get('/{professional_family}/professione_istat', ['as' => 'professione_istat', 'uses' => 'ReportController@professioneISTAT']);        
    Route::get('/{professional_family}/salario', ['as' => 'salario', 'uses' => 'ReportController@salario']);
    Route::get('/{professional_family}/confronto', ['as' => 'confronto', 'uses' => 'ReportController@confronto']);
});

Route::group([ 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.' , 'middleware' => ['auth' , 'can:is_admin']] , function() {
    
    /** Users */
    Route::group([ 'prefix' => 'users', 'as' => 'users.'] , function() {
        Route::get('/index', ['as' => 'index', 'uses' =>'UserController@index']);
        Route::get('/create', ['as' => 'create', 'uses' =>'UserController@create']);
        Route::post('/store', ['as' => 'store', 'uses' =>'UserController@store']);
        Route::get('/{user_id}/edit', ['as' => 'edit', 'uses' =>'UserController@edit']);        
        Route::put('/update', ['as' => 'update', 'uses' =>'UserController@update']);
        Route::delete('/delete', ['as' => 'delete', 'uses' =>'UserController@delete']);
        Route::get('/{user_id}/reset_password', ['as' => 'reset_password', 'uses' =>'UserController@resetPassword']);
        Route::post('/change_password', ['as' => 'change_password', 'uses' =>'UserController@changePassword']);
    });
    
    /** Professional Families */
    Route::group([ 'prefix' => 'professional_families', 'as' => 'professional_families.'] , function() {
        Route::get('/index', ['as' => 'index', 'uses' =>'ProfessionalFamilyController@index']);
        Route::get('/create', ['as' => 'create', 'uses' =>'ProfessionalFamilyController@create']);
        Route::post('/store', ['as' => 'store', 'uses' =>'ProfessionalFamilyController@store']);
        Route::get('/{professional_family_id}/edit', ['as' => 'edit', 'uses' =>'ProfessionalFamilyController@edit']);
        Route::put('/update', ['as' => 'update', 'uses' =>'ProfessionalFamilyController@update']);
        Route::delete('/delete', ['as' => 'delete', 'uses' =>'ProfessionalFamilyController@delete']);
    });

    /** Professional Families */
    Route::group([ 'prefix' => 'imports', 'as' => 'imports.'] , function() {
        Route::get('/index', ['as' => 'index', 'uses' =>'ImportController@index']);
        Route::get('/create', ['as' => 'create', 'uses' =>'ImportController@create']);
        Route::post('/store', ['as' => 'store', 'uses' =>'ImportController@store']);        
    });    
    
});