@extends('layout.app')


@section('app-css')

    <style>
        .navbar-collapse .nav-item{
            bakcground-color: white;
            text-align: center;
            border: 1px solid white;
            background-color: #343a40;
            color: white;
        }

        .navbar-light .navbar-nav.navbar-reports .nav-item{
            background-color:#3D4448;
        }

        .navbar-light .navbar-nav.navbar-reports .nav-link {
            color: rgb(255, 255, 255);            
        }        

        .navbar-light .navbar-nav .active > .nav-link, .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .nav-link.show, .navbar-light .navbar-nav .show > .nav-link {
            color: #00A9DD;
        }

        .bg-light, .bg-light > a {            
            padding: 0 0 40px 0;
        }

        .navbar-light .navbar-toggler {
            color: white;
            border-color: rgba(0,0,0,.1);
            background-color: #343a40;
        }

        ul.navbar-nav{
            margin-top: 10px;
        }
    </style>    

    @yield('report-css')
@endsection

@section('content_header')    
    <h3><i class="{{ \App\Models\ProfessionalFamily::where('slug', $professional_family)->first()->icon }}"></i> {{ \App\Models\ProfessionalFamily::where('slug', $professional_family)->first()->name }}</h3>
    @include('layout.menu_reports')    
    @yield('app_content_header')
@endsection

@section('app-content')    
    @yield('report-content')
@endsection

@section('app-js')
    @yield('report-js')
@endsection

