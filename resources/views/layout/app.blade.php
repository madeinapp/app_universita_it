@extends('adminlte::page')

@section('css')
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/public/css/app.css">    
    @yield('app-css')
@endsection

@section('content')

    @if (session('msg'))
        <div class="alert alert-success">
            {{ session('msg') }}
        </div>
    @endif

    @yield('app-content')

@endsection

@section('js')

    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $(".alert").fadeOut('slow');
            }, 5000);
        });
    </script>

    @yield('app-js')

@endsection