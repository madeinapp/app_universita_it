<section>
    <nav class="navbar navbar-expand-lg navbar-reports navbar-light bg-light">        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="{{ $professional_family_icon }}"></i> Menù reports
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav navbar-reports">
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*andamento*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/andamento">Annunci di mercato</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*professioni*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/professioni">Professioni</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*aziende*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/aziende">Aziende</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*esperienza*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/esperienza">Esperienza</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*contratto*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/contratto">Tipo Contratto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*bilancio_competenze*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/bilancio_competenze">Bilancio competenze</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*top_competenze*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/top_competenze">Top 10 competenze</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*regioni*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/regioni">Regioni</a>
                </li>
                {{-- 
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*geo_annunci_prov*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/geo_annunci_prov">Provincia</a>
                </li>                                
                 --}}
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*salario*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/salario">Salario</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('*confronto*') ? 'active' : '' }}" href="/reports/{{ $professional_family }}/confronto">Confronto</a>
                </li>
            </ul>
        </div>
    </nav>
</section>