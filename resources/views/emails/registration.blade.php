Queste sono le tue credenziali per l'accesso a UniSKit
<br>
<br>Email: <strong>{{ $user->email }}</strong>
<br>Password: <strong>{{ $password }}</strong>

<br><br>
Per accedere clicca <a href="https://app.universita.it/login">qui</a>
<br><br>
Buono studio!
<br>
Universita.it