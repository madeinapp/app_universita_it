@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Salario</h1>
    <p>Qual è il salario offerto per gli sbocchi professionali possibili attraverso questo percorso di laurea?</p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 40vh;
  width: 40vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
<div class="row">
    <section class="col-lg-12">
        <div class="card card-default">            
            <div class="card-body">
                <div class="row">
                    <div class="col-md-7">
                        @if( !empty(array_values($data)) )
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div> 
                        @else
                        <h4>Nessun dato disponibile</h4>
                        @endif
                    </div>
                    <div class="col-md-5">
                        <table class="table table-bordered">
                            <thead>
                                <th>Salario</th>
                                <th>% Annunci</th>
                            </thead>
                            <tbody>
                                @foreach($data as $salario => $tot)
                                <tr>
                                    <td>{{ $salario }}</td>
                                    <td>{{ $tot }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div><!-- /.card-body -->
        </div>
    </section>
</div>
<div class="row">
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){    
        
        $("#regione").on('change', function(){            
            $('form').submit();
        });
                                        
        var chartCanvas = document.getElementById('myChart');
        var options = {
                maintainAspectRatio: false,
                legend: {
                    position: (window.innerWidth>window.innerHeight) ? 'left' : 'top'
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            };

        if( window.innerHeight>window.innerWidth ){            
            chartCanvas.height = '600px';
            options = {                   
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            };
        }   
                 
        var ctx = chartCanvas.getContext('2d');     

        if( ctx ){
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    datasets: [{
                        data: {!! json_encode(array_values($data)) !!},
                        backgroundColor: [
                            "rgba(255, 99, 132, 0.8)",
                            "rgba(36, 65, 185, 0.8)",
                            "rgba(252, 163, 64, 0.8)",
                            "rgba(224, 233, 63, 0.8)",
                            "rgba(45, 223, 26, 0.8)",
                            "rgba(43, 94, 2, 0.8)",
                            "rgba(61, 126, 200, 0.8)",
                            "rgba(175, 157, 209, 0.8)",
                            "rgba(208, 251, 126, 0.8)",
                            "rgba(47, 235, 94, 0.8)",
                            "rgba(167, 74, 143, 0.8)",
                            "rgba(119, 55, 16, 0.8)"
                        ],
                    }],
                    
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: {!! json_encode(array_keys($data)) !!}
                },
                options: options
            });
        }            
    });
</script>
@endsection