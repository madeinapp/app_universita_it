@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Professioni</h1>
    <p>Quali sono i principali sbocchi professionali possibili con questo tipo di laurea e come sono distribuiti?</p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 40vh;
  width: 40vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    width: 80vw;
    }
}
</style>
@endsection


@section('report-content')
<div class="row">

    <section class="col-lg-12">
        <div class="card card-default">
            
            <div class="card-body">

                <div class="row">
                    <div class="col-md-7">
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div>  
                    </div>
                    <div class="col-md-5">
                        <table id="data-table" class="table table-bordered">
                            <thead>
                                <th>Voce di mercato</th>
                                <th>%</th>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->detailocc_label }}</td>
                                    <td>{{ round(($row->tot_voce_di_mercato * 100 ) / $totale , 2) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>    
                    </div>
                </div>                                                                                        
                    
            </div><!-- /.card-body -->
        </div>
    </section>
</div>
<div class="row">
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){        
        var chartCanvas = document.getElementById('myChart');
        var options = {
                reponsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: (window.innerWidth>window.innerHeight) ? 'left' : 'top'
                }
            };
        if( window.innerHeight>window.innerWidth ){            
            chartCanvas.height = '600px';
            
        }            
        var ctx = chartCanvas.getContext('2d');          
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [{{ $values }}],
                    backgroundColor: [
                        "rgb(255, 99, 132)",
                        "rgb(36, 65, 185)",
                        "rgb(252, 163, 64)",                        
                        "rgb(45, 223, 26)",
                        "rgb(43, 94, 2)",
                        "rgb(61, 126, 200)",
                        "rgb(175, 157, 209)",
                        "rgb(208, 251, 126)",
                        "rgb(47, 235, 94)",
                        "rgb(225, 98, 142)"
                    ],
                }],
                
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [{!! $labels !!}]
            },
            options: options
        });

        /*
        $('#data-table').DataTable({
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            paging:   false,
            order: [[ 1, "desc" ]],
        });
        */
        
    });
</script>
@endsection