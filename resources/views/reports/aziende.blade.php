@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Aziende</h1>
    <p>Quali aziende pubblicano la maggior parte degli annunci di lavoro?</p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 80vh;
  width: 40vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
<div class="row">

    <section class="col-lg-12">

        <!-- Add a svg area, empty -->
        <section id="graph"></section>


        <div class="card card-default">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-md-7">
                        
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div>                        
                        
                    </div>

                    <div class="col-md-5">
                        <table id="data-table" class="table table-bordered">
                            <thead>
                                <th>aziende</th>
                                <th>% Annunci</th>
                            </thead>
                            <tbody>
                                @foreach ($data as $row)
                                <tr>
                                    <td>{{ $row->companyraw }}</td>
                                    <td>{{ $row->totale }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>                                    
            </div>                                    
        </div>
    </section>
</div>
<div class="row">
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){                        

        var chartCanvas = document.getElementById('myChart');                
        var ctx = chartCanvas.getContext('2d');   
        var values = [{{ $values }}];        

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [{!! $labels !!}],                
                datasets: [{
                    label: '# Annunci',
                    data: values,
                    backgroundColor: [
                        "rgb(255, 99, 132, 0.2)",
                        "rgb(36, 65, 185, 0.2)",
                        "rgb(252, 163, 64, 0.2)",
                        "rgb(225, 98, 142, 0.2)",
                        "rgb(45, 223, 26, 0.2)",
                        "rgb(43, 94, 2, 0.2)",
                        "rgb(61, 126, 200, 0.2)",
                        "rgb(175, 157, 209, 0.2)",
                        "rgb(76, 21, 21, 0.2)",
                        "rgb(47, 235, 94, 0.2)"
                    ],
                    borderColor: [
                        "rgb(255, 99, 132, 1)",
                        "rgb(36, 65, 185, 1)",
                        "rgb(252, 163, 64, 1)",
                        "rgb(225, 98, 142, 1)",
                        "rgb(45, 223, 26, 1)",
                        "rgb(43, 94, 2, 1)",
                        "rgb(61, 126, 200, 1)",
                        "rgb(175, 157, 209, 1)",
                        "rgb(76, 21, 21, 1)",
                        "rgb(47, 235, 94, 1)"
                    ],
                    borderWidth: 1
                }]
            },
            options: {                
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {                    
                    xAxes: [{  
                        ticks: {                            
                            autoskip: false,
                            autoSkipPadding: 0,
                            minRotation: 90,     
                            fontSize: 14,                       
                        },                      
                        display: true,                         
                        gridLines: {
                            display:true
                        },
                        scaleLabel: {
                            display: true,                            
                            labelString: "aziende",
                            fontSize: 18,
                        }                        
                    }],
                    yAxes: [{                        
                        display: true,
                        gridLines: {                            
                            display:true
                        },
                        ticks: {
                            fontSize: 14,
                        },
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Numero di annunci",
                        },                            
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.datasets[0].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            }
        });        
        
        /*
        $('#data-table').DataTable({
            language: {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json"
            },
            paging: false,
            order: [[ 1, "desc" ]]
        });
        */
    });
</script>
@endsection