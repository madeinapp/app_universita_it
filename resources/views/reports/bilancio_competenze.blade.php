@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Bilancio competenze</h1>
    <p>
        Per ogni percorso professionale, negli annunci di lavoro quanto sono richieste le competenze tecnico-specialistiche (hard skills) rispetto alle competenze trasversali (soft skills)? 
        E quante sono le competenze digitali richieste (digital skills) rispetto alle competenze non digitali?
    </p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 40vh;
  width: 40vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 40vh;
    width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
<div class="row">    
    @foreach($data as $val)
    <div class="col-12">        
        <div class="card">
            <div class="card-header text-center">
                <h5>{{ $val->vdm }}</h5>
            </div>
            <div class="card-body">                                                                                  
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center">      
                        
                        <div class="chart-container">                                               
                            <canvas id="{{ $val->slug }}_0" class="chart-canvas" width="500" height="200"></canvas>                                                                        
                        </div>
                    </div>                
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 text-center">                                                
                        <div class="chart-container">                                               
                            <canvas id="{{ $val->slug }}_2" class="chart-canvas" width="500" height="200"></canvas>                                                                        
                        </div>
                    </div>
                </div>        
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){        
                        
        var data = {!! json_encode($data) !!};   
        
        console.log(data);

        var charts = [];
        for(var i=0; i<data.length; i++){
            charts.push(
                [
                    data[i].slug+'_0',
                    data[i].slug+'_2'
                ]
            );
        }
            
        console.log(charts);

        for(var i=0; i<charts.length; i++){
            var chart = charts[i][0];            

            var myChart = [];
            
            var chartCanvas = document.getElementById(chart);
            console.log(screen.width,screen.height);
            if( screen.height>screen.width ){                                            
                chartCanvas.width = '500px';
                chartCanvas.height = '600px';
            }            
            var ctx = chartCanvas.getContext('2d');   

            myChart[chart] = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [data[i].hard, data[i].soft],
                        backgroundColor: [
                            "rgba(255, 99, 132, 1)",
                            "rgba(0, 169, 221, 1)",                            
                        ],                     
                    }],
                    
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: ['Hard', 'Soft']
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        position: (window.innerWidth>window.innerHeight) ? 'left' : 'top'
                    },									
					responsive: true,					
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {                            
                                var label = data.datasets[0].data[tooltipItem.index] + "%";
                                return label;
                            }
                        }
                    }
				}
            });


            chart = charts[i][1];            

            var myChart = [];
            var chartCanvas = document.getElementById(chart);
            if( screen.height>screen.width ){   
                chartCanvas.width = '500px';         
                chartCanvas.height = '600px';
            }            
            var ctx = chartCanvas.getContext('2d'); 
            myChart[chart] = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [data[i].digital, data[i].no_digital],
                        backgroundColor: [                            
                            "rgba(39, 235, 63, 0.8)",
                            "rgba(252, 163, 64, 1)"
                            
                        ]
                    }],
                    
                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: ['Digital', 'No digital']
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        position: (window.innerWidth>window.innerHeight) ? 'left' : 'top'
                    },										
					responsive: true,	
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {                            
                                var label = data.datasets[0].data[tooltipItem.index] + "%";
                                return label;
                            }
                        }
                    }				
				}
            });
        }
	
    });
</script>
@endsection

