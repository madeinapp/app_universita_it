@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Annunci di mercato</h1>
    <p>Qual è l’andamento degli annunci di mercato pubblicati per  questo percorso di laurea?</p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 80vh;
  width: 80vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 40vh;
    width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
</nav>
<div class="row">
    <div class="col-md-12">
        <div class="card card-default">
            <div class="card-body">                
                <div class="chart-container">
                    <canvas id="myChart"></canvas>
                </div>                
            </div>
        </div>                
    </div>
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){        
        var chartCanvas = document.getElementById('myChart');
        if( window.innerHeight>window.innerWidth ){            
            chartCanvas.height = '600px';
        }            
        var ctx = chartCanvas.getContext('2d');   
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: {!! json_encode(array_keys($data)) !!},
                datasets: [{
                    label: '# di Annunci',
                    data: {!! json_encode(array_values($data)) !!},
                    backgroundColor: 'rgba(0, 169, 221, 0.5)',
                    borderColor: 'rgba(0, 169, 221, 1)',                        
                    borderWidth: 1
                }]
            },
            options: {
                maintainAspectRatio: false,                
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Numero di annunci",
                        },   
                        ticks: {
                            beginAtZero: true
                        }
                    }],                    
                    xAxes: [{                          
                        scaleLabel: {
                            display: true,                            
                            labelString: "Mese",
                            fontSize: 18,
                        },
                        ticks: {
                            autoskip: false,
                            autoSkipPadding: 0,
                            minRotation: 90,     
                            fontSize: 14,
                        }                   
                    }],
                }               
            }
        });
    });
</script>
@endsection

