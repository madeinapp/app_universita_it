@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Confronto</h1>
    <p>Confronta i principali dati di due percorsi di laurea</p>
@stop


@section('report-css')
<style>
.chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    width: 80vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
        position: relative;
        margin: auto;
        height: 80vh;
        width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
</nav>
{!! Form::open(['method' => 'GET']) !!}

<div class="row">    
    <div class="col-md-6">
        <div class="form-group">            
            {!! Form::label('vs', 'Confronta con...') !!}
            {!! Form::select('vs', $elenco_vs, $professional_family_vs, ['class' => 'form-control sel-vdm']) !!}
        </div>
    </div>          
</div>
{!! Form::close() !!}
<div class="row">
    <div class="col-md-12">        
        <div class="card card-default">
            <div class="card-body">                                                                      
                <div class="chart-container">
                    <canvas id="chart_annunci"></canvas>
                </div>
            </div>
        </div>                
        <div class="card card-default">
            <div class="card-body">                                               
                <div class="chart-container">
                    <canvas id="chart_esperienza"></canvas>
                </div>                  
            </div>
        </div>  
        <div class="card card-default">
            <div class="card-body">                                                   
                <div class="chart-container">
                    <canvas id="chart_contratto"></canvas>
                </div>                  
            </div>
        </div>        
        <div class="card card-default">
            <div class="card-body">                                                                   
                <div class="chart-container">
                    <canvas id="chart_salario"></canvas>
                </div>  
            </div>
        </div>      
    </div>
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){        

        $(".sel-vdm").on('change', function(){
            $("form").submit();
        });        

        /* Andamento */
        var chartCanvas = document.getElementById('chart_annunci');
              
        var ctx = chartCanvas.getContext('2d');   
        var myChart = new Chart(ctx, {            
            type: 'line',
            data: {
                labels: {!! json_encode(array_keys($data_annunci)) !!},
                datasets: [
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[0]; }, $data_annunci))) !!},
                        backgroundColor: 'rgb(255, 99, 132, 0.5)',
                        borderColor: 'rgb(255, 99, 132, 1)',
                        borderWidth: 1
                    },
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family_vs]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[1]; }, $data_annunci))) !!},
                        backgroundColor: 'rgba(0, 169, 221, 0.5)',                                               
                        borderColor: 'rgba(0, 169, 221, 1)',                                                
                        borderWidth: 1
                    }]
            },
            options: {
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Andamento Annunci',
                    fontSize: 24
                }, 
                legend: {
                    display: true,
                    labels:{
                        fontSize: 14
                    }                    
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Mese",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Numero di annunci",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }]
                }
            }
        });


        /* Esperienza */
        var chartCanvas = document.getElementById('chart_esperienza');
               
        var ctx = chartCanvas.getContext('2d');   
        var myChart = new Chart(ctx, {            
            type: 'horizontalBar',
            data: {
                labels: {!! json_encode(array_keys($data_esperienza)) !!},
                datasets: [
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[0]; }, $data_esperienza))) !!},
                        backgroundColor: 'rgb(255, 99, 132, 0.8)',
                        borderColor: 'rgb(255, 99, 132, 1)',
                        borderWidth: 1
                    },
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family_vs]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[1]; }, $data_esperienza))) !!},
                        backgroundColor: 'rgba(0, 169, 221, 0.5)',                                               
                        borderColor: 'rgba(0, 169, 221, 1)',                                          
                        borderWidth: 1
                    }]
            },
            options: {
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Esperienza richiesta',
                    fontSize: 24
                }, 
                legend: {
                    display: true,
                    labels:{
                        fontSize: 14
                    }
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Anni di esperienza",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "% annunci",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            }
        });




        /* Contratto */
        var chartCanvas = document.getElementById('chart_contratto');
        
        var ctx = chartCanvas.getContext('2d');   
        var myChart = new Chart(ctx, {            
            type: 'horizontalBar',
            data: {
                labels: {!! json_encode(array_keys($data_contratto)) !!},
                datasets: [
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[0]; }, $data_contratto))) !!},
                        backgroundColor: 'rgb(255, 99, 132, 0.8)',
                        borderColor: 'rgb(255, 99, 132, 1)',
                        borderWidth: 1
                    },
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family_vs]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[1]; }, $data_contratto))) !!},
                        backgroundColor: 'rgba(0, 169, 221, 0.5)',                                               
                        borderColor: 'rgba(0, 169, 221, 1)',                                  
                        borderWidth: 1
                    }]
            },
            options: {
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Contratti offerti',
                    fontSize: 24
                }, 
                legend: {
                    display: true,
                    labels:{
                        fontSize: 14
                    }
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Tipologia contratto",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "% annunci",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            }
        });





        /* Salario */
        var chartCanvas = document.getElementById('chart_salario');
        
        var ctx = chartCanvas.getContext('2d');   
        var myChart = new Chart(ctx, {            
            type: 'horizontalBar',
            data: {
                labels: {!! json_encode(array_keys($data_salario)) !!},
                datasets: [
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[0]; }, $data_salario))) !!},
                        backgroundColor: 'rgb(255, 99, 132, 0.8)',
                        borderColor: 'rgb(255, 99, 132, 1)',
                        borderWidth: 1
                    },
                    {
                        label: '{!! addslashes($elenco_famiglie_professionali[$professional_family_vs]) !!}',
                        data: {!! json_encode(array_values(array_map(function($v){ return $v[1]; }, $data_salario))) !!},
                        backgroundColor: 'rgba(0, 169, 221, 0.5)',                                               
                        borderColor: 'rgba(0, 169, 221, 1)',                                    
                        borderWidth: 1
                    }]
            },
            options: {
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Salario offerto',
                    fontSize: 24
                }, 
                legend: {
                    display: true,
                    labels:{
                        fontSize: 14
                    }
                },
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Salario",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "% annunci",
                        },   
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                                                                       
                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            }
        });
    });
</script>
@endsection

