@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
    <h1 class="m-0 text-dark">Annunci di lavoro per Provincia</h1>
@stop

@section('report-css')
<link rel="stylesheet" media="all" href="/css/jquery-jvectormap-2.0.5.css"/>

<style>
    .chart-container {
        position: relative;
        margin: auto;
        height: 60vh;
        width: 20vw;                
    }    

    #map{
        width: 100%; 
        height: 505px;
    }

    @media screen and (max-width: 567px) {
        /* regole CSS */
        .chart-container {
            position: relative;
            margin: auto;
            height: 80vh;
            width: 80vw;
        }
    }
    
    @media (max-width: 768px) {  
        #map{        
            height: 400px;        
        }
    }
</style>
@endsection

@section('report-content')
<div class="card card-default">
    <div class="card-body">
        <div class="row">
            <div class="col-md-4 text-center map-container">                
                <p>Seleziona una regione</p>
                <div id="map"></div>            
            </div>
            <div class="col-md-5">                
                <div class="chart-container">                            
                    <canvas id="myChart"></canvas>                    
                </div>                             
            </div>
            <div class="col-md-3">                                
                <table class="table">
                    <thead>
                        <tr>
                            <th>Voce di Mercato</th>
                            <th>Annunci</th>
                        </tr>
                    </thead>
                    @foreach($data_vdm as $k => $val)
                    <tr>
                        <td>{{ $k }}</td>
                        <td>{{ $val }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>                
@endsection

@section('report-js')
<script src="/js/jquery-jvectormap-2.0.5.min.js"></script>
<script src="/js/jquery-jvectormap-it-merc.js"></script>
<script>
    $(function(){            
                
        var values = {!! json_encode($data) !!};

        var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
            isMobile = true;
        }

        $('#map').vectorMap({
            map: 'it_merc',
            backgroundColor: '#fff',
            container: $('#map'),                      
            series: {
                regions: [{                          
                    scale: ['#C8EEFF', '#006491'],
                    normalizeFunction: 'polynomial',
                    values: values
                }]
            },
            onRegionTipShow: function(e, el, code){
                el.html(
                    el.html()+' <br> (' + (( typeof(values[code]) !== 'undefined') ? values[code] : '0') +' annunci)'
                );
            },
            onRegionClick: function(element, code, region)
            {        
                if( typeof(values[code]) !== 'undefined') {
                    document.location.href="?provincia="+code.toUpperCase();            
                }
            }
        });

        var values_vdm = {!! json_encode(array_values($data_vdm)) !!};     
        var chartCanvas = document.getElementById('myChart');        
        var viewport = $("#viewport").val();            
        var chart_type = 'doughnut';        

        /*
        if( !isMobile && values_vdm.length > 3 ){
            chart_type = 'radar';
        } 
        */

        var options = {
                maintainAspectRatio: false,
                legend: {
                    display: chart_type == 'radar' ? false : true,
                },
                title: {
                    display: true,
                    text: '{!! addslashes(strtoupper($elenco_province[$provincia])) !!}',
                    fontSize: 24
                },					
                responsive: true,					
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {          
                            console.log(data);
                            if( typeof data.datasets[0].data[tooltipItem.index] !== 'undefined' ){
                                //var label = data.labels[tooltipItem.index] + '\n' + data.datasets[0].data[tooltipItem.index] + " annunci";
                                var label = data.datasets[0].data[tooltipItem.index] + " annunci";
                            }else{
                                var label = "Nessun annuncio";
                            }
                            return label;
                        }
                    }
                }                
            };
            
        if( chart_type == 'radar' ){            
            options['scale'] = {
                pointLabels: {
                    fontSize: 20
                }
            }
        }  
                    
        var ctx = chartCanvas.getContext('2d');                  
        

        myChart = new Chart(ctx, {
            type: chart_type,
            data: {
                datasets: [{
                    data: values_vdm,
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.8)",
                        "rgba(36, 65, 185, 0.8)",
                        "rgba(252, 163, 64, 0.8)",
                        "rgba(225, 98, 142, 0.8)",
                        "rgba(45, 223, 26, 0.8)",
                        "rgba(43, 94, 2, 0.8)",
                        "rgba(61, 126, 200, 0.8)",
                        "rgba(175, 157, 209, 0.8)",
                        "rgba(76, 21, 21, 0.8)",
                        "rgba(47, 235, 94, 0.8)"
                    ],
                    borderColor: [
                        "rgba(255, 99, 132, 1)",
                        "rgba(36, 65, 185, 1)",
                        "rgba(252, 163, 64, 1)",
                        "rgba(225, 98, 142, 1)",
                        "rgba(45, 223, 26, 1)",
                        "rgba(43, 94, 2, 1)",
                        "rgba(61, 126, 200, 1)",
                        "rgba(175, 157, 209, 1)",
                        "rgba(76, 21, 21, 1)",
                        "rgba(47, 235, 94, 1)"
                    ],
                }],                    
                
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: {!! json_encode(array_keys($data_vdm)) !!}
            },
            options: options            
        });        
               
    });
</script>
@endsection