@extends('layout.report')

@section('title', config('app.name') )

@section('app_content_header')
<h1 class="m-0 text-dark">Tipo contratto</h1>
<p>Che tipo di contratto viene offerto negli annunci di lavoro?</p>
@stop

@section('report-css')
<style>
.chart-container {
  position: relative;
  margin: auto;
  height: 80vh;
  width: 40vw;
}

@media screen and (max-width: 567px) {
    /* regole CSS */
    .chart-container {
    position: relative;
    margin: auto;
    height: 60vh;
    width: 80vw;
    }
}
</style>
@endsection

@section('report-content')
<div class="row">

    @if((new \Jenssegers\Agent\Agent())->isMobile())
    <input type="hidden" id="chart_type" value="bar">
    @else
    <input type="hidden" id="chart_type" value="horizontalBar">
    @endif

    <section class="col-lg-12">
        <div class="card card-default">            
            <div class="card-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="chart-container">
                            <canvas id="myChart"></canvas>
                        </div> 
                    </div>
                    <div class="col-md-5">        
                        <table id="data-table" class="table table-bordered">
                            <thead>
                                <th>Contratto</th>
                                <th>% Annunci</th>
                            </thead>
                            <tbody>
                                @foreach ($data as $k => $row)
                                <tr>
                                    <td>{!! $row->contract !!}</td>
                                    <td>{{ $values[$k] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>                    
                </div>
            </div><!-- /.card-body -->
        </div>
    </section>
</div>
<div class="row">
</div>
@endsection

@section('report-js')
<script>
    $(document).ready(function(){       

        var chartType = document.getElementById('chart_type').value;        
        
        var chartCanvas = document.getElementById('myChart');                  
        var ctx = chartCanvas.getContext('2d');                        
        var myChart = new Chart(ctx, {
            type: chartType,
            data: {
                labels: {!! json_encode($labels) !!},
                datasets: [{
                    label: '# Annunci',
                    data: {!! json_encode($values) !!},
                    backgroundColor: [
                        "rgb(255, 99, 132, 0.2)",
                        "rgb(36, 65, 185, 0.2)",
                        "rgb(252, 163, 64, 0.2)",
                        "rgb(225, 98, 142, 0.2)",
                        "rgb(45, 223, 26, 0.2)",
                        "rgb(43, 94, 2, 0.2)",
                        "rgb(61, 126, 200, 0.2)",
                        "rgb(175, 157, 209, 0.2)",
                        "rgb(76, 21, 21, 0.2)",
                        "rgb(47, 235, 94, 0.2)"
                    ],
                    borderColor: [
                        "rgb(255, 99, 132, 1)",
                        "rgb(36, 65, 185, 1)",
                        "rgb(252, 163, 64, 1)",
                        "rgb(225, 98, 142, 1)",
                        "rgb(45, 223, 26, 1)",
                        "rgb(43, 94, 2, 1)",
                        "rgb(61, 126, 200, 1)",
                        "rgb(175, 157, 209, 1)",
                        "rgb(76, 21, 21, 1)",
                        "rgb(47, 235, 94, 1)"
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontSize: 14,
                        },
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Tipologia di contratto",
                        }

                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            fontSize: 18,
                            labelString: "Annunci in percentuale",
                        },
                        ticks: {                            
                            autoskip: false,
                            autoSkipPadding: 0,
                            minRotation: 90,     
                            fontSize: 14,                       
                        }, 
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {                            
                            var label = data.datasets[0].data[tooltipItem.index] + "%";
                            return label;
                        }
                    }
                }
            }
        });        
        
        
    });
</script>
@endsection