@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">                
                <div class="card-body">
                    <div class="row"> 
                        <div class="col-2">
                            <img src="https://www.universita.it/wp-content/uploads/2020/06/cartoon-hipster-8-web.png" style="width: 100%; text-align: center; float:left;">
                        </div>
                        <div class="col-10">
                            Benvenuto/a su <strong>UniSKit</strong>, l'unica piattaforma in Italia che ti permette di scegliere il corso di laurea giusto per te.

                            <p>
                            Vediamo subito come puoi usare questa piattaforma.
                            <br>Nel menu a tendina troverai tutti i percorsi di studio che puoi analizzare.                    
                            <br>Selezionane uno e scopri i dati del mercato del lavoro italiano per il 2020.
                            </p>
        
                            <p>
                                Puoi analizzare:
                                <ul>
                                    <li>L’andamento storico degli annunci di lavoro</li>
                                    <li>Gli sbocchi professionali più frequenti </li> 
                                    <li>Le aziende che stanno assumendo</li>
                                    <li>L'esperienza richiesta dai datori di lavoro</li> 
                                    <li>Il tipo di contratto che puoi aspettarti</li> 
                                    <li>La percentuale di competenze trasversali e digitali necessarie</li> 
                                    <li>Le 10 competenze specifiche più importanti </li> 
                                    <li>La distribuzione sul territorio degli annunci di lavoro</li> 
                                </ul>
                            </p>
        
                            <p>L'ultima tab “Confronti" permette di confrontare i dati con un altro percorso di studi.</p>
        
                            <p>Hai dubbi o domande su come usare UniSKit? Scrivici a <a href="mailto:info@universita.it">info@universita.it</a>.</p>
                        </div>
                    </div>                                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
