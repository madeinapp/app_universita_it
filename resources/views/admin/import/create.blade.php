@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Importa CSV</h1>
    <p>Il file deve risiedere nella cartella /storage/app/csv</p>
@stop

@section('app-content')
<div class="row">
    <div class="col-md-12">        
        {!! Form::open(array('route' => array('admin.imports.store'), 'method' => 'POST' )) !!}                                    

            <div class="form-group">
                {!! Form::label('filename', 'Nome file') !!}
                {!! Form::select('filename', $files, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('professional_family_id', 'Famiglia professionale') !!}
                {!! Form::select('professional_family_id', $professional_families, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('pre_import', 'Pre-import') !!}
                {!! Form::select('pre_import', ['Crea la tabella se non esiste o svuotala se già presente', 'Aggiungi i dati a quelli esistenti'], null, ['class' => 'form-control']) !!}
            </div>
        
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-success">Importa</button>
                <a href="{{ route('admin.imports.index') }}" class="btn btn-lg btn-default">Annulla</a>
            </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection