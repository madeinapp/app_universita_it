@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Importazioni CSV</h1>
@stop

@section('app-content')
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <a href="{{ route('admin.imports.create') }}" class="btn btn-lg btn-primary"><i class="fas fa-file-import"></i> Nuova importazione</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Eseguito da</th>
                    <th>Famiglia professionale</th>
                    <th>File</th>
                    <th>Righe importate</th>
                    <th>Data importazione</th>                    
                </tr>
            </thead>
            <tbody>
            @foreach($imports as $import)
                <tr>
                    <td>{{ $import->user->name }} {{ $import->user->surname }}</td>
                    <td>{{ $import->professional_family->name }}</td>
                    <td>{{ $import->filename }}</td>
                    <td>{{ $import->rows }}</td>
                    <td>{{ $import->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $imports->links() }}
    </div>
</div>
        
@endsection

@section('app-js')
<script>                    
</script>
@endsection