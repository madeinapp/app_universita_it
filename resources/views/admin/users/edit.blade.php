@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Modifica utente</h1>
@stop

@section('css')
<style>
.professional_families_list{
    list-style-type: none;
}

.professional_families_list input[type=checkbox]{
    cursor: pointer;
}
</style>
@endsection

@section('app-content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-md-12">        
        {!! Form::open(array('route' => array('admin.users.update', $user->id))) !!}
        
            @method('PUT')

            {!! Form::hidden('action', $action) !!}            
            {!! Form::hidden('user_id', $user->id) !!}

            @include('admin.users.fields')

            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-success">Salva</button>
                <a href="{{ route('admin.users.index') }}" class="btn btn-lg btn-default">Annulla</a>
            </div>
        
        {!! Form::close() !!}
    </div>
</div>
@endsection