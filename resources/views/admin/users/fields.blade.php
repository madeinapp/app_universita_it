<div class="card">            
    <div class="card-body">                                                                            
        <div class="row">
            @if( $user )
            <div class="form-group col-12">                                
                {!! Form::radio('status', 1, $user->status == 1) !!}
                {!! Form::label('status', 'Disabilitato') !!}
                
                {!! Form::radio('status', 2, $user->status == 2) !!}
                {!! Form::label('status', 'Abilitato') !!}
            </div>
            @endif

            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('name', 'Nome') !!}
                {!! Form::text('name', ($user) ? $user->name : null, ['class'=>'form-control']) !!}
                @error('name')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('surname', 'Cognome') !!}
                {!! Form::text('surname', ($user) ? $user->surname : null, ['class'=>'form-control']) !!}
                @error('surname')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', ($user) ? $user->email : null, ['class'=>'form-control']) !!}
                @error('email')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('role_id', 'Ruolo') !!}
                {!! Form::select('role_id', $roles, ($user) ? $user->role_id : null, ['class'=>'form-control']) !!}
                @error('role_id')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>            
    </div>
</div>        

@if( $action == 'create' )
<div class="card">            
    <div class="card-body">                   
        <div class="form-group">
            <h3>                
                Password                
            </h3>                            
        </div>                                                         
        <div class="row">           
            <div class="col-lg-6"> 
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class'=>'form-control']) !!}
                    @error('password')
                        <span class="badge badge-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-lg-6"> 
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Conferma Password') !!}
                    {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                    @error('password_confirmation')
                        <span class="badge badge-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>            
    </div>
</div>
@endif

<div class="card">            
    <div class="card-body">                   
        <div class="form-group">
            <h3>Famiglie Professionali</h3>    
        </div>                                                         
        <div class="row">            
            <div class="col-lg-6">
                @error('professional_family_id')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror    
                <ul class="professional_families_list">
                @foreach ($professional_families as $professionalFamily)
                    <li>                    
                        {!! Form::checkbox('professional_family_id[]', $professionalFamily->id, in_array($professionalFamily->id, $user_professional_families)) !!}
                        {!! Form::label('professional_family_id', $professionalFamily->name) !!}
                    </li>
                @endforeach
                </ul>
            </div>                        
        </div>            
    </div>
</div>        