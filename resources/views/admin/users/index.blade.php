@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Utenti</h1>
@stop

@section('app-content')
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="btn-group">            
                <a href="{{ route('admin.users.create') }}" class="btn btn-lg btn-primary"><i class="fa fa-user-plus"></i> Nuovo utente</a>                
                <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-filter"></i> Filtri
                </button>
            </div>

            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <form method="GET">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('filter-name', 'Nome') !!}
                                    {!! Form::text('filter-name', $filter_name, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('filter-surname', 'Cognome') !!}
                                    {!! Form::text('filter-surname', $filter_surname, ['class' => 'form-control']) !!}
                                </div>
                            </div>                        
                        </div>      
                        <div class="form-group">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success">Cerca</button>
                                <a href="{{ route('admin.users.index') }}" class="btn btn-secondary">Elimina filtri</a>
                            </div>
                        </div>              
                    </form>
                </div>
            </div>
        </div>

        <div class="card">            
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <th></th>                            
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Ruolo</th>
                            <th>Email</th>
                            <th>Stato</th>
                            <th>Famiglie Professionali</th>
                        </thead>
                        <tbody>
                            @foreach ( $users as $k => $user )
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.users.edit', ['user_id' => $user->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>                                            
                                            <a href="{{ route('admin.users.reset_password', ['user_id' => $user->id]) }}" class="btn btn-warning btn-sm"><i class="fas fa-key"></i></a>                                            
                                            <button type="button" onclick="deleteUser({{ $user->id }},'{{ $user->name }} {{ $user->surname }}');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </td>                                    
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->surname }}</td>
                                    <td>
                                        @if( $user->role->name == 'Amministratore' )
                                            <h5><span class="badge badge-primary"><i class="fas fa-user-cog"></i> Amministratore</span></h5>
                                        @elseif( $user->role->name == 'Studente' )
                                            <h5><span class="badge badge-warning"><i class="fas fa-user-graduate"></i> Studente</span></h5>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        @if( $user->status == 1)
                                        <h5><span class="badge badge-danger">Disabilitato</span></h5>
                                        @elseif( $user->status == 2)
                                        <h5><span class="badge badge-success">Attivo</span></h5>
                                        @endif
                                    </td>
                                    <td>
                                        @foreach($user->professional_families as $pf)
                                            <i class="{{ $pf->icon }}" title="{{ $pf->name }}"></i>                                            
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer clearfix">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-are-you-sure" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Elimina utente</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Eliminare <span id="selected_value"></span>?</p>
            </div>
            <div class="modal-footer justify-content-between">
                {!! Form::open(array('route' => array('admin.users.delete'), 'id' => 'frm_users')) !!}

                    @method('DELETE')

                    <input type="hidden" name="user_id" id="user_id" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                    <button type="submit" class="btn btn-danger">Elimina</button>

                {!! Form::close() !!}                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('app-js')
<script>
            
    function deleteUser (id, name){                        
        $("#modal-are-you-sure #user_id").val(id);
        $("#modal-are-you-sure #selected_value").text(name);
        $("#modal-are-you-sure").modal('show');            
    };
    
</script>
@endsection