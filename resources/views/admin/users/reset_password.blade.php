@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Reset password utente</h1>
@stop

@section('app-content')
{!! Form::open(array('route' => array('admin.users.change_password'), 'method' => 'POST')) !!}
<div class="card">            
    <div class="card-body">                   
        <div class="form-group">
            <h3>                
                Cambia la password per: {{ $user->name }} {{ $user->surname }}
            </h3>                            
        </div>                                                         
        <div class="row">           
            <div class="col-lg-6"> 
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class'=>'form-control']) !!}
                    @error('password')
                        <span class="badge badge-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="col-lg-6"> 
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Conferma Password') !!}
                    {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                    @error('password_confirmation')
                        <span class="badge badge-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>          
        <div class="row">
            <div class="col-12">
                Password consigliata: {{ $suggested_password }}
            </div>    
        </div>  
    </div>
</div>

<div class="form-group">
    {!! Form::hidden('user_id', $user->id) !!}
    <div class="form-group">
        <button type="submit" class="btn btn-lg btn-success">Salva</button>
        <a href="{{ route('admin.users.index') }}" class="btn btn-lg btn-default">Annulla</a>
    </div>
</div>

{!!  Form::close() !!}
@endsection