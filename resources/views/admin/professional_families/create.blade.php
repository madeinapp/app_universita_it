@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Nuova Famiglia Professionale</h1>
@stop

@section('app-content')
<div class="row">
    <div class="col-md-12">        
        {!! Form::open(array('route' => array('admin.professional_families.store'), 'method' => 'POST')) !!}                        

            {!! Form::hidden('action', 'store') !!}

            @include('admin.professional_families.fields')
        
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-success">Salva</button>
                <a href="{{ route('admin.professional_families.index') }}" class="btn btn-lg btn-default">Annulla</a>
            </div>

        {!! Form::close() !!}
    </div>
</div>
@endsection