@extends('layout.app')

@section('title', config('app.name') )

@section('content_header')
    <h1 class="m-0 text-dark">Famiglie Professionali</h1>
@stop

@section('app-content')
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <a href="{{ route('admin.professional_families.create') }}" class="btn btn-lg btn-primary"><i class="fa fa-user-md"></i> Nuova famiglia professionale</a>
        </div>
        
        @if($professional_families->count())
        <div class="card">            
            <div class="card-body">
                <div class="table-responsive">                    
                   
                    <table class="table table-striped">
                        <thead>
                            <th></th>                            
                            <th>Famiglia Professionale</th>                            
                            <th>Testo menu</th>                            
                            <th>Icona</th>                            
                            <th>Tabella dati</th>                                                                          
                        </thead>
                        <tbody>
                            @foreach ( $professional_families as $k => $professional_family )
                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('admin.professional_families.edit', ['professional_family_id' => $professional_family->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                                            <button type="button" onclick="deleteProfessionalFamily({{ $professional_family->id }},'{{ $professional_family->name }}');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </td>                                    
                                    <td>{{ $professional_family->name }}</td>       
                                    <td>{{ $professional_family->menu_text }}</td>                                     
                                    <td>
                                        @if( $professional_family->icon )
                                        <i class="{{ $professional_family->icon }}">
                                        @endif
                                    </td>
                                    <td>
                                        {{ $professional_family->slug }}
                                    </td>                                   
                                </tr>
                            @endforeach
                        </tbody>
                    </table>                     
                </div>
            </div>
        </div>
        @else
            <h1><span class="badge badge-warning">Non esistono famiglie professionali</span></h1>
        @endif
    </div>
</div>

<div class="modal fade" id="modal-are-you-sure" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Elimina famiglia professionale</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Eliminare <span id="selected_value"></span>?</p>
            </div>
            <div class="modal-footer justify-content-between">
                {!! Form::open(array('route' => array('admin.professional_families.delete'), 'id' => 'frm_professional_family')) !!}

                    @method('DELETE')

                    <input type="hidden" name="professional_family_id" id="professional_family_id" value="">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                    <button type="submit" class="btn btn-danger">Elimina</button>

                {!! Form::close() !!}                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('app-js')
<script>
            
    function deleteProfessionalFamily (id, name){                        
        $("#modal-are-you-sure #professional_family_id").val(id);
        $("#modal-are-you-sure #selected_value").text(name);
        $("#modal-are-you-sure").modal('show');            
    };
    
</script>
@endsection