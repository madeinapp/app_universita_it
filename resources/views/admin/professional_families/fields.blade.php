<div class="card">            
    <div class="card-body">                                                                            
        <div class="row">
            
            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('name', 'Nome') !!}
                {!! Form::text('name', ($professional_family) ? $professional_family->name : null, ['class'=>'form-control']) !!}
                @error('name')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>         
            
            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('menu_text', 'Testo menu') !!} <small>(max 25 caratteri)</small>
                {!! Form::text('menu_text', ($professional_family) ? $professional_family->menu_text : null, ['maxlength' => 25, 'class'=>'form-control']) !!}                
                @error('menu_text')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>
            
            <div class="form-group col-12 col-sm-12 col-md-6 col-lg-6">
                {!! Form::label('icon', 'Icona') !!}
                {!! Form::text('icon', ($professional_family) ? $professional_family->icon : null, ['class'=>'form-control']) !!}
                <span>Utilizzare le classi di <a href="https://fontawesome.com/icons?d=gallery">fontawesome</a>. Es.: fas fa-archway</span>                
                @error('icon')
                    <span class="badge badge-danger">{{ $message }}</span>
                @enderror
            </div>  
        </div>            
    </div>
</div>  