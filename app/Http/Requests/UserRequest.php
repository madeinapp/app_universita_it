<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('is_admin');
    }
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {                    
        
        $userId = $this->user_id ? $this->user_id : null;
        
        return [
            'user_id' => 'sometimes|required',
            'name' => 'required|min:3|max:255',
            'surname' => 'nullable|min:3|max:255',            
            'email' => 'unique:users,email,'. $userId,                        
            'role_id' => 'required',
            'status' =>'sometimes|required',
            'professional_family_id' => 'required'
        ];
    }

    protected function getValidatorInstance() {
        $validator = parent::getValidatorInstance();
        
        $validator->sometimes('password', 'required|confirmed|min:6', function($input) {
            return $input->action == 'create';
        });

        /*
        $validator->sometimes('professional_family_id', 'required', function($input) {
            return $input->role_id > 1 ;
        });
        */

        return $validator;
    }
}
