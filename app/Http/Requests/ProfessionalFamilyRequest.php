<?php

namespace App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Foundation\Http\FormRequest;

class ProfessionalFamilyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('is_admin');
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {                
        $this->merge([
            'slug' => Str::slug($this->name),
        ]);       
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        

        $professionalFamilyId = $this->professional_family_id ? $this->professional_family_id : null;

        return [
            'professional_family_id' => 'sometimes|required',
            'name' => 'required|min:3|max:255|unique:professional_families,name,'.$professionalFamilyId,
            'slug' => 'required',
            'icon' => 'required',
            'menu_text' => 'required|max:25'
        ];
    }
}
