<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\ImportRequest;

use App\Repository\ImportRepositoryInterface;
use App\Repository\ProfessionalFamilyRepositoryInterface;

use File;
use DB;

class ImportController extends Controller
{

    private $importRepository; 
    private $professionalFamilyRepository; 

    public function __construct(ImportRepositoryInterface $importRepository, ProfessionalFamilyRepositoryInterface $professionalFamilyRepository)
    {        
        $this->importRepository = $importRepository;
        $this->professionalFamilyRepository = $professionalFamilyRepository;
    }

    public function index()
    {
        $imports = $this->importRepository->all();
        return view('admin.import.index', [
            'imports' => $imports
        ]);
    }

    public function create(){

        $professional_families = $this->professionalFamilyRepository->all()->pluck('name', 'id');

        $files = Storage::disk('local')->files('csv');        

        $files = array_map(function($v){ return basename($v); }, $files);
        
        return view('admin.import.create', [
            'professional_families' => $professional_families,
            'files' => array_combine(array_values($files), $files)
        ]);

    }

    public function store(ImportRequest $request)
    {
                    
        $professional_family = $this->professionalFamilyRepository->find($request['professional_family_id']);                

        $this->importRepository->createTable($professional_family->slug, $request['pre_import']);

        $path = storage_path('app/csv/'.$request['filename']);      
        
        //$data = array_map('str_getcsv', file($path));        
        $ins_rows = 0;
        
        if(File::exists($path)):                        
            
            $header = [];
            $data = array();            

            if (($handle = fopen($path, 'r')) !== FALSE):
            
                while (($row = fgetcsv($handle, 1000, ',')) !== FALSE)
                {
                    if(!$header):
                        $header = $row;
                    else:
                        $data_row = array_combine($header, $row);
                        
                        if( DB::table($professional_family->slug)->insert($data_row) ):
                            $ins_rows++;
                        endif;

                    endif;
                }
                fclose($handle);
            endif;
                  
        endif;

        $this->importRepository->create([
            'user_id' => $request['user_id'],
            'professional_family_id' => $professional_family->id,
            'rows' => $ins_rows,
            'filename' => $request['filename']
        ]);            
        
    

        return redirect(route('admin.imports.index'));
    }                
   
}
