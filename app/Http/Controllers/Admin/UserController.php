<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;
use App\Repository\UserRepositoryInterface;
use App\Repository\RoleRepositoryInterface;
use App\Repository\ProfessionalFamilyRepositoryInterface;
use App\Repository\UserProfessionalFamilyRepositoryInterface;

use App\Http\Requests\UserRequest;
use App\Http\Requests\ChangePasswordRequest;

class UserController extends Controller
{
    private $userRepository;
    private $roleRepository;
    private $professionalFamilyRepository;
    private $userProfessionalFamilyRepository;
  
    public function __construct(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository, ProfessionalFamilyRepositoryInterface $professionalFamilyRepository, UserProfessionalFamilyRepositoryInterface $userProfessionalFamilyRepository)
    {        
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->professionalFamilyRepository = $professionalFamilyRepository;
        $this->userProfessionalFamilyRepository = $userProfessionalFamilyRepository;
    }

    public function index(Request $request)
    {
        $filter_name = $request->query('filter-name');
        $filter_surname = $request->query('filter-surname');

        $users = $this->userRepository->all($filter_name, $filter_surname);

        return view('admin.users.index', [
            'users' => $users,
            'filter_name' => $filter_name,
            'filter_surname' => $filter_surname,
        ]);
    }

    public function create(){
        
        $user = null;

        $roles = $this->roleRepository->all()->pluck('name', 'id');

        $status_list = array('Disabilitato', 'Attivo');

        $professionalFamilies = $this->professionalFamilyRepository->all();

        $user_professional_families = [];
        
        return view('admin.users.create')->with('user', $user)
                                       ->with('roles', $roles)
                                       ->with('action', 'create')
                                       ->with('status_list', $status_list)
                                       ->with('professional_families', $professionalFamilies)
                                       ->with('user_professional_families', $user_professional_families)
                                       ;
    }

    public function store(UserRequest $request){
        $input = $request->validated();        
               
        $user_professional_families_attributes = [];
        if( isset($input['professional_family_id'])):
            $professional_families_id = $input['professional_family_id'];
            unset($input['professional_family_id']);                        
        endif;               

        $user_attributes = $input;

        $user_attributes['password'] = Hash::make($user_attributes['password']);

        /** Aggiorno i dati utente */                
        $res = $this->userRepository->create($user_attributes);        
        if($res):                

            $user_id = $res->id;

            foreach($professional_families_id as $id):
                $user_professiona_family_id = [
                    'user_id' => $user_id,
                    'professional_family_id' => $id
                ];

                $user_professional_families_attributes[] = $user_professiona_family_id;
            endforeach;


            /** Aggiorno le famiglie professionali dell'utente */
            $this->userProfessionalFamilyRepository->deleteByUser($user_id);                                
            foreach($user_professional_families_attributes as $attributes):                
                $res = $this->userProfessionalFamilyRepository->create($attributes);
            endforeach;                                    
        
        endif;

        return redirect(route('admin.users.index'))->with(['msg'=>'Utente creato con successo!']);
        
    }    

    public function edit($user_id){
        $user = $this->userRepository->find($user_id);

        $roles = $this->roleRepository->all()->pluck('name', 'id');

        $status_list = array('Disabilitato', 'Attivo');

        $professionalFamilies = $this->professionalFamilyRepository->all();

        $user_professional_families = $this->userRepository->professionalFamilies($user_id);
        
        return view('admin.users.edit')->with('user', $user)
                                       ->with('roles', $roles)
                                       ->with('action', 'edit')
                                       ->with('status_list', $status_list)
                                       ->with('professional_families', $professionalFamilies)
                                       ->with('user_professional_families', $user_professional_families)
                                       ;
    }

    public function update(UserRequest $request){
        $input = $request->validated();        
        
        $user_id = $input['user_id'];
        unset($input['user_id']);

        $user_professional_families_attributes = [];
        if( isset($input['professional_family_id'])):
            $professional_families_id = $input['professional_family_id'];
            unset($input['professional_family_id']);
            
            foreach($professional_families_id as $id):
                $user_professiona_family_id = [
                    'user_id' => $user_id,
                    'professional_family_id' => $id
                ];

                $user_professional_families_attributes[] = $user_professiona_family_id;
            endforeach;

        endif;               

        $user_attributes = $input;

        /** Aggiorno i dati utente */                
        $res = $this->userRepository->find($user_id)->update($user_attributes);        
        if($res):                
            /** Aggiorno le famiglie professionali dell'utente */
            $this->userProfessionalFamilyRepository->deleteByUser($user_id);                                
            foreach($user_professional_families_attributes as $attributes):                
                $res = $this->userProfessionalFamilyRepository->create($attributes);
            endforeach;                                    
        
        endif;

        return redirect(route('admin.users.index'))->with(['msg'=>'Utente aggiornato con successo!']);
        
    }

    public function delete(Request $request){
        $input = $request->only('user_id');
        
        $this->userRepository->find($input['user_id'])->delete();

        return redirect(route('admin.users.index'))->with(['msg'=>'Utente eliminato']);    
    }


    public function resetPassword($user_id)
    {
        $user = $this->userRepository->find($user_id);

        if( $user ):

            $suggested_password = Str::random(12);

            return view('admin.users.reset_password', [
                'user' => $user,
                'suggested_password' => $suggested_password
            ]);
        else:
            return redirect(route('admin.users.index'))->withErrors(['msg' => 'Utente inesistente']);
        endif;
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        
        $res = $this->userRepository->find($request['user_id'])->update(['password' => Hash::make($request['password'])]);
        if( $res ):
            return redirect(route('admin.users.index'))->with(['msg' => 'Password modificata']);
        else: 
            return redirect(route('admin.users.index'))->with(['msg' => 'Si è verificato un errore durante il cambio password']);
        endif;
    }
}
