<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfessionalFamilyRequest;
use App\Repository\ProfessionalFamilyRepositoryInterface;

class ProfessionalFamilyController extends Controller
{
    private $professionalFamilyRepository;
  
    public function __construct(ProfessionalFamilyRepositoryInterface $professionalFamilyRepository)
    {        
        $this->professionalFamilyRepository = $professionalFamilyRepository;
    }

    public function create(){
        return view('admin.professional_families.create', [
            'professional_family' => null
        ]);
    }

    public function store(ProfessionalFamilyRequest $request){
        $input = $request->validated();
        
        $professionalFamily = $this->professionalFamilyRepository->create($input);
        if($professionalFamily):
            return redirect(route('admin.professional_families.index'));
        else:
            return back()->withErrors(['msg' => 'Error creating Professional Family']);
        endif;
    }

    public function index(){
        $professional_families = $this->professionalFamilyRepository->all();
        
        /*
        $rows = [];
        foreach($professional_families as $pf):
            $rows[$pf->slug] = $this->professionalFamilyRepository->rows($pf->slug);
        endforeach;
        */
        
        return view('admin.professional_families.index', [
            'professional_families' => $professional_families
        ]);
    }

    public function edit($professional_family_id){
        $professional_family = $this->professionalFamilyRepository->find($professional_family_id);
        
        return view('admin.professional_families.edit')->with('professional_family', $professional_family)                                       
                                       ;
    }

    public function update(ProfessionalFamilyRequest $request){
        $input = $request->validated();        
        $professional_family_id = $input['professional_family_id'];
        unset($input['professional_family_id']);
        
        $this->professionalFamilyRepository->find($professional_family_id)->update($input);

        return redirect(route('admin.professional_families.index'));                                       
    }

    public function delete(Request $request){
        $input = $request->only('professional_family_id');
        
        $this->professionalFamilyRepository->find($input['professional_family_id'])->delete();

        return redirect(route('admin.professional_families.index'))->with(['msg'=>'Famiglia professionale eliminata']);    
    }
}
