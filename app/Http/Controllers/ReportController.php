<?php

namespace App\Http\Controllers;

use App\Repository\ProfessionalFamilyRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Repository\ReportRepositoryInterface;


class ReportController extends Controller
{
    private $reportRepository;
    
    public function __construct(ReportRepositoryInterface $reportRepository, ProfessionalFamilyRepositoryInterface $professionalFamilyRepository)
    {        
        $this->reportRepository = $reportRepository;        
        $this->professionalFamilyRepository = $professionalFamilyRepository;        
    }

    public function getProfessionalFamilyIcon($professional_family_slug)
    {
        $professionalFamily = $this->professionalFamilyRepository->findBySlug($professional_family_slug);

        $professional_family_icon = '';
        if( $professionalFamily ): 
            $professional_family_icon = $professionalFamily->icon;            
        endif;

        return $professional_family_icon;
    }

    public function andamento($professional_family)
    {                        
        $this->reportRepository->setProfessionalFamily($professional_family);        
        
        $data = $this->reportRepository->andamento();     

        ksort($data);        
                
        return view('reports.andamento', [            
            'data' => $data,
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function aziende($professional_family)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);

        $data = $this->reportRepository->aziende();        

        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_aziende();                  
        
        $totale = 0;
        if( isset($tot_annunci[0]->totale) ): 
            $totale = $tot_annunci[0]->totale;
        endif;                
        
        $labels = [];
        $values = [];

        foreach($data as $k => $row): 
            $labels[] = "'".$row->companyraw."'";            
            $perc = round(( $row->totale * 100 ) / $totale, 2);
            $values[] = $perc;        
            $data[$k]->totale = $perc;
        endforeach;   
                
        return view('reports.aziende', [
            'data' => $data,
            'labels' => implode(",",$labels),
            'values' => implode(",",$values),
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function bilancio_competenze($professional_family)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);        

        $data = $this->reportRepository->bilancio_competenze();

        foreach($data as $k => $val):    
            $data[$k]->vdm = ucfirst($data[$k]->vdm);
            $data[$k]->slug = Str::slug($data[$k]->vdm);            
        endforeach;
        
        return view('reports.bilancio_competenze', [
            'data' => $data,
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function esperienza($professional_family)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);

        $data = $this->reportRepository->esperienza();       
        
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_esperienza();        
        
        $totale = 0;
        if( isset($tot_annunci[0]->totale) ): 
            $totale = $tot_annunci[0]->totale;
        endif;        
        
        $labels = [];
        $values = [];
        
        foreach($data as $k => $row): 
            $data[$k]->experience = ucfirst($row->experience);
            $labels[] = ucfirst($row->experience);
            if( $totale > 0 ):
                $values[] = round(($row->tot_experience * 100 ) / $totale , 2);
            else:
                $values[] = 0;
            endif;
        endforeach;  

        /*
        dump($data);
        dump($labels);
        dd($values);
        */
                
        return view('reports.esperienza', [
            'data' => $data,        
            'totale' => $totale,
            'labels' => $labels,
            'values' => $values,    
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function contratto($professional_family)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);

        $data = $this->reportRepository->contratto();          
        
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_contratto();        
        
        $totale = 0;
        if( isset($tot_annunci[0]->totale) ): 
            $totale = $tot_annunci[0]->totale;
        endif;                   
        
        $labels = [];
        $values = [];
        
        foreach($data as $k => $row):             
            $data[$k]->contract = ucfirst($row->contract);
            $labels[] = ucfirst($row->contract);
            if( $totale > 0 ):
                $values[] = round(($row->tot_contract * 100 ) / $totale , 2);
            else:
                $values[] = 0;
            endif;
        endforeach;  
                
        return view('reports.contratto', [
            'data' => $data,    
            'totale' => $totale,
            'labels' => $labels,
            'values' => $values,            
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function top_competenze($professional_family)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);

        $voci_di_mercato = $this->reportRepository->elenco_voci_di_mercato();              

        //$competenze = $this->reportRepository->elenco_competenze();          

        foreach($voci_di_mercato as $vdm): 

            $tot_annunci = $this->reportRepository->tot_annunci_vdm($vdm);        
            $totale = 0;
            if( isset($tot_annunci[0]->totale) ): 
                $totale = $tot_annunci[0]->totale;
            endif; 

            $perc_competenze[Str::slug($vdm)]['name'] = ucfirst($vdm);

            $top_competenze = $this->reportRepository->top_competenze($vdm);            

            foreach($top_competenze as $k => $val): 
                if( empty($k) ): 
                    unset($top_competenze[$k]);                                      
                else:
                    $top_competenze[$k] = round(( $top_competenze[$k] * 100 ) / $totale, 2);
                endif;
            endforeach;
            
            if( count($top_competenze) > 0 ):               
                $perc_competenze[Str::slug($vdm)]['values'] = $top_competenze;
            else:
                unset($perc_competenze[Str::slug($vdm)]);
            endif;
            
        endforeach;                   

        return view('reports.top_competenze', [            
            'data' => $perc_competenze,
            'data_json' => json_encode($perc_competenze),
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)     
        ]);
        
    }    
           
    public function professioni($professional_family){
        $this->reportRepository->setProfessionalFamily($professional_family);

        $data = $this->reportRepository->professioni();        
        $tot_annunci = $this->reportRepository->tot_annunci();        
        $totale = 0;
        if( isset($tot_annunci[0]->totale) ): 
            $totale = $tot_annunci[0]->totale;
        endif;        
        
        $labels = [];
        $values = [];
        
        foreach($data as $k => $row): 
            $data[$k]->detailocc_label = ucfirst($row->detailocc_label);
            $labels[] = "'".ucfirst($row->detailocc_label)."'";
            if( $totale > 0 ):
                $values[] = round(($row->tot_voce_di_mercato * 100 ) / $totale , 2);
            else:
                $values[] = 0;
            endif;
        endforeach;                     
                
        return view('reports.professioni', [
            'data' => $data,
            'totale' => $totale,
            'labels' => implode(",",$labels),
            'values' => implode(",",$values),
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function regioni($professional_family, Request $request)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);

        $data = $this->reportRepository->regioniioni();                            
      
        $elenco_regioni = $this->reportRepository->elenco_regioni();              
        
        $totale = array_sum($data);                

        foreach($data as $k => $val):             
            $data[$k] = round(( $val * 100 ) / $totale, 2);
        endforeach;
        
        $regione = $request->query('regione');

        if( !$regione ): 

            $regione = array_keys($elenco_regioni)[0];

        endif;        
        
        $data_vdm = $this->reportRepository->geo_vdm_regioni($regione);                
        $totale_vdm = array_sum($data_vdm);
        
        foreach ($data_vdm as $key => $val) {            
            unset($data_vdm[$key]);
            $data_vdm[ucfirst($key)] = round(($val*100)/$totale_vdm, 2);
        }

        return view('reports.regioni', [
            'data' => $data,       
            'data_vdm' => $data_vdm,       
            'elenco_regioni' => $elenco_regioni,
            'regione' => $regione,
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }

    public function geo_annunci_prov($professional_family, Request $request)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);
        
        $data = $this->reportRepository->geo_annunci_province();                    

        $elenco_province = $this->reportRepository->elenco_province();                        

        $provincia = $request->query('provincia');

        if( !$provincia ): 

            $provincia = array_keys($elenco_province)[0];

        endif;  

        $data_vdm = $this->reportRepository->geo_vdm_province($provincia);                

        foreach ($data_vdm as $key => $val) {
            unset($data_vdm[$key]);
            $data_vdm[ucfirst($key)] = $val;
        }
        
        return view('reports.geo_annunci_prov', [
            'data' => $data,  
            'data_vdm' => $data_vdm,       
            'provincia' => $provincia,             
            'elenco_province' => $elenco_province,
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }    

    public function salario($professional_family, Request $request)
    {
        $this->reportRepository->setProfessionalFamily($professional_family);        
                                     
        $salario = $this->reportRepository->annunci_salario_italia();         
        
        $totale_annunci = array_sum($salario);
                        
        foreach($salario as $k => $val): 
            $salario[$k] = number_format( round( ( $val * 100 ) / $totale_annunci , 2) , 2);
        endforeach;        
                        
        array_multisort(array_keys($salario), SORT_NATURAL, $salario);
                
        return view('reports.salario', [                        
            'data' => $salario,                    
            'professional_family' => $professional_family,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }
    
    public function confronto($professional_family, Request $request)
    {                
        $elenco_famiglie_professionali = $this->professionalFamilyRepository->all()->pluck('menu_text', 'slug')->toArray();
                
        $professional_family_vs = $request->query('vs');        
        
        $elenco_vs = $elenco_famiglie_professionali;
        unset($elenco_vs[$professional_family]);
        
        if( !$professional_family_vs ):            
            $professional_family_vs = array_keys($elenco_vs)[0];
        endif;                

        /** Annunci */
        $this->reportRepository->setProfessionalFamily($professional_family);    
        $annunci1 = $this->reportRepository->confronto_annunci();
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $annunci2 = $this->reportRepository->confronto_annunci();        

        //$ultimi_12_mesi = array_values(array_map(function($v){ return $v->mese; } ,$this->reportRepository->ultimi_12_mesi() ));        
        
        $mesi = array_keys($annunci1);        
        natsort($mesi);
        
        $data_annunci = [];
        foreach($mesi as $mese):  
            $data_annunci[$mese][0] = isset( $annunci1[$mese] ) ? $annunci1[$mese] : 0;
            $data_annunci[$mese][1] = isset( $annunci2[$mese] ) ? $annunci2[$mese] : 0;
        endforeach;
        
        
        /** Esperienza */
        $tot_annunci = 0;

        $this->reportRepository->setProfessionalFamily($professional_family);    
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_esperienza();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_esperienza1 = $tot_annunci[0]->totale;
        endif;  
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_esperienza();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_esperienza2 = $tot_annunci[0]->totale;
        endif;  
        
        $this->reportRepository->setProfessionalFamily($professional_family);    
        $esperienza1 = $this->reportRepository->confronto_esperienza($professional_family);        
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $esperienza2 = $this->reportRepository->confronto_esperienza($professional_family_vs);        
       
        $arr_esperienza = array_unique(array_merge(array_keys($esperienza1), array_keys($esperienza2)));        
        $data_esperienza = [];
        foreach($arr_esperienza as $esperienza): 
            $data_esperienza[$esperienza][0] = isset( $esperienza1[$esperienza] ) ? round(($esperienza1[$esperienza]*100)/$tot_esperienza1, 2) : 0;
            $data_esperienza[$esperienza][1] = isset( $esperienza2[$esperienza] ) ? round(($esperienza2[$esperienza]*100)/$tot_esperienza2, 2) : 0;
        endforeach;        


        /** Contratto */
        $tot_contratto = 0;

        $this->reportRepository->setProfessionalFamily($professional_family);
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_contratto();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_contratto1 = $tot_annunci[0]->totale;
        endif;
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_contratto();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_contratto2 = $tot_annunci[0]->totale;
        endif;
        //dump($tot_contratto);        

        $this->reportRepository->setProfessionalFamily($professional_family);    
        $contratto1 = $this->reportRepository->confronto_contratto($professional_family);
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $contratto2 = $this->reportRepository->confronto_contratto($professional_family_vs);        

        //dump($contratto1);
        //dump($contratto2);
       
        $arr_contratto = array_unique(array_merge(array_keys($contratto1), array_keys($contratto2)));
        $data_contratto = [];
        foreach($arr_contratto as $contratto): 
            $data_contratto[$contratto][0] = isset( $contratto1[$contratto] ) ? round(($contratto1[$contratto]*100)/$tot_contratto1, 2) : 0;
            $data_contratto[$contratto][1] = isset( $contratto2[$contratto] ) ? round(($contratto2[$contratto]*100)/$tot_contratto2, 2) : 0;
        endforeach;     
        //dd($data_contratto);

        /** Salario */
        $tot_annunci = 0;
        $this->reportRepository->setProfessionalFamily($professional_family); 
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_salario();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_salario1 = $tot_annunci[0]->totale;
        endif;
        $this->reportRepository->setProfessionalFamily($professional_family_vs); 
        $tot_annunci = $this->reportRepository->tot_annunci_ultimi_12_salario();
        if( isset($tot_annunci[0]->totale) ): 
            $tot_salario2 = $tot_annunci[0]->totale;
        endif;

        $this->reportRepository->setProfessionalFamily($professional_family);    
        $salario1 = $this->reportRepository->confronto_salario($professional_family);
        $this->reportRepository->setProfessionalFamily($professional_family_vs);    
        $salario2 = $this->reportRepository->confronto_salario($professional_family_vs);        
       
        $arr_salario = array_unique(array_merge(array_keys($salario1), array_keys($salario2)));
        natsort($arr_salario);

        $data_salario = [];
        foreach($arr_salario as $salario): 
            $data_salario[$salario][0] = isset( $salario1[$salario] ) ? round(($salario1[$salario]*100)/$tot_salario1, 2) : 0;
            $data_salario[$salario][1] = isset( $salario2[$salario] ) ? round(($salario2[$salario]*100)/$tot_salario2, 2) : 0;
        endforeach;
        
        
        return view('reports.confronto', [                        
            'elenco_famiglie_professionali' => $elenco_famiglie_professionali,            
            'elenco_vs' => $elenco_vs,            
            'data_annunci' => $data_annunci,
            'data_esperienza' => $data_esperienza,
            'data_contratto' => $data_contratto,
            'data_salario' => $data_salario,
            'professional_family' => $professional_family,
            'professional_family_vs' => $professional_family_vs,
            'professional_family_icon' => $this->getProfessionalFamilyIcon($professional_family)
        ]);
    }
}
