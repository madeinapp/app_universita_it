<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

use App\Repository\UserRepositoryInterface;
use Mail;
use Log;

use App\Models\User;
use App\Models\ProfessionalFamily;
use App\Models\UserProfessionalFamily;
use App\Repository\ProfessionalFamilyRepositoryInterface;
use App\Repository\UserProfessionalFamilyRepositoryInterface;

class UserController extends Controller
{
    private $userRepository;
    private $professionalFamilyRepository;
    private $userProfessionalFamilyRepository;

    public function __construct(UserRepositoryInterface $userRepository, ProfessionalFamilyRepositoryInterface $professionalFamilyRepository, UserProfessionalFamilyRepositoryInterface $userProfessionalFamilyRepository)
    {
        $this->userRepository = $userRepository;
        $this->professionalFamilyRepository = $professionalFamilyRepository;
        $this->userProfessionalFamilyRepository = $userProfessionalFamilyRepository;
    }

    public function register(Request $request)
    {
        /*
        $handle = fopen('php://input', 'r');        
        $json = fgets($handle);        
        
        if(!empty($json)):
            $contact = json_decode($json, true)["contact"];
        endif;
        */

        $input = $request->all(); 
        Log::debug('User register input: ' . json_encode($input));              
        $contact = $input["contact"];            
    
        $validator = Validator::make($contact, [
            'email' => 'required|email',            
            'first_name' => 'required|string|min:2|max:255',            
            'last_name' => 'nullable|string|min:2|max:255',            
        ]);

        if ($validator->fails()) {
            Log::debug('User register params ERROR');    
            return response()->json(array('status'=>0, 'msg'=>'Missing required inputs'), 401);
        }                            
        
        Log::debug('User register params OK');    

        $check = User::where('email', $contact['email'])->first();
        if( $check ): 
            return response()->json(array('status' => 'error' , 'msg' => 'Email già presente'), 200);
        endif;
        
        $password = Str::random(8);       

        $new_user = [
            'name' => $contact['first_name'],
            'surname' => $contact['last_name'],
            'email' => $contact['email'],
            'password' => Hash::make($password)
        ];

        $user = $this->userRepository->create($new_user);       
        
        if( $user ): 
            Log::debug('User registration succeded!');    
            $professionalFamilies = $this->professionalFamilyRepository->all();
            foreach($professionalFamilies as $professionalFamily): 
                $this->userProfessionalFamilyRepository->create([
                    'user_id' => $user->id,
                    'professional_family_id' => $professionalFamily->id
                ]);
            endforeach;
        endif;

        try{
            Mail::send('emails.registration', ['user' => $user, 'password' => $password], function ($m) use ($user) {
                $m->from('info@universita.it', 'UniSKit');    
                $m->to($user->email, $user->name)->subject('Credenziali UniSKit');
            });            
            Log::debug('Email sent');    
        }catch(\Exception $e){
            Log::debug('Email error: ' . $e->getMessage());    
        }
    }
}
