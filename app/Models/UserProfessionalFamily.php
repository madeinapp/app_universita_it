<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfessionalFamily extends Model
{
    public $table = 'users_professional_families';
        
    protected $fillable = [
        'id',
        'user_id',
        'professional_family_id'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [        
        'id' => 'integer',
        'user_id'  => 'integer',
        'professional_family_id'  => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'professional_family_id' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function professional_family()
    {
        return $this->belongsTo(\App\Models\ProfessionalFamily::class);
    }

}
