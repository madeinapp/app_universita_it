<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessionalFamily extends Model
{
    protected $fillable = [
        'id',
        'name',
        'slug',
        'icon',
        'menu_text'
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [        
        'id' => 'integer',
        'name'  => 'string',
        'slug'  => 'string',
        'icon'  => 'string',
        'menu_text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:professional_families|min:3|max:255',
        'slug' => 'required|unique:professional_families|min:3|max:255',
        'menu_text' => 'required|max:25'
    ];

}
