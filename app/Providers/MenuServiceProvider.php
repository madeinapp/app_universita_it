<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use App\Repository\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Contracts\Events\Dispatcher;

class MenuServiceProvider extends ServiceProvider
{    

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repository\UserRepositoryInterface', 
            'App\Repository\Eloquent\UserRepository'
        );
    }   
    

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Dispatcher $events, UserRepositoryInterface $userRpository)
    {           

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) use($userRpository) {                                                

            $user = $userRpository->find(Auth::id());                 
            
            $event->menu->add('REPORTS');

            $professional_families = $user->professional_families;           
        
            foreach( $professional_families as $professional_family): 

                $event->menu->add(
                    [
                        'key' => $professional_family->slug,
                        'text' => $professional_family->menu_text,                    
                        'icon_color' => 'white',
                        'icon' => $professional_family->icon,
                        'url' => '/reports/' . $professional_family->slug . '/andamento',
                        'active' => ['/reports/' . $professional_family->slug.'/*'
                    ]
                ]);                  
                                                                              
            endforeach;
        });
    }
}
