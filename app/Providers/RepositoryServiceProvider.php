<?php

namespace App\Providers;

use App\Repository\EloquentRepositoryInterface; 
use App\Repository\UserRepositoryInterface; 
use App\Repository\RoleRepositoryInterface; 
use App\Repository\ProfessionalFamilyRepositoryInterface; 
use App\Repository\UserProfessionalFamilyRepositoryInterface; 
use App\Repository\ReportRepositoryInterface; 
use App\Repository\ImportRepositoryInterface; 

use App\Repository\Eloquent\UserRepository; 
use App\Repository\Eloquent\BaseRepository; 
use App\Repository\Eloquent\RoleRepository; 
use App\Repository\Eloquent\ProfessionalFamilyRepository; 
use App\Repository\Eloquent\UserProfessionalFamilyRepository; 
use App\Repository\Eloquent\ReportRepository; 
use App\Repository\Eloquent\ImportRepository;


use Illuminate\Support\ServiceProvider; 

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(ProfessionalFamilyRepositoryInterface::class, ProfessionalFamilyRepository::class);
        $this->app->bind(UserProfessionalFamilyRepositoryInterface::class, UserProfessionalFamilyRepository::class);
        $this->app->bind(ReportRepositoryInterface::class, ReportRepository::class);
        $this->app->bind(ImportRepositoryInterface::class, ImportRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
