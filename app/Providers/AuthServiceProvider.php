<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is_admin', function ($user) {
            return $user->role->name == 'Amministratore';
        });

        Gate::define('is_student', function ($user) {
            return $user->role->name == 'Studente';
        });

        Gate::define('architettura', function ($user) {
            
            $found = false;
            
            foreach($user->professional_families as $pf):
                if($pf->professional_family->slug == 'architettura'):
                    $found = true;
                    break;
                endif;
            endforeach;

            return $found;
        });
    }
}
