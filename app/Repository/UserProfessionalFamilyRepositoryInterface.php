<?php
namespace App\Repository;

use Illuminate\Support\Collection;

interface UserProfessionalFamilyRepositoryInterface
{
   public function all(): Collection;

   public function deleteByUser($user_id): bool;
   
}