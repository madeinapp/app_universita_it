<?php
namespace App\Repository;

use Illuminate\Database\Eloquent\Collection;

interface ReportRepositoryInterface
{
   public function all(): Collection;   

   public function tot_righe();

   public function ultimi_12_mesi();

   public function elenco_voci_di_mercato();

   public function elenco_competenze();

   public function elenco_regioni();

   public function elenco_province();

   public function setProfessionalFamily($professionalFamily);

   public function andamento();

   public function tot_annunci();
   public function tot_annunci_ultimi_12_mesi();
   public function tot_annunci_ultimi_12_aziende();
   public function tot_annunci_ultimi_12_esperienza();
   public function tot_annunci_ultimi_12_contratto();
   public function tot_annunci_ultimi_12_salario();
   public function tot_annunci_vdm($vdm);

   public function professioni();

   public function aziende();

   public function esperienza();

   public function contratto();
   
   public function bilancio_competenze();
   
   public function top_competenze($voce_di_mercato);

   public function geo_annunci_province();
   public function regioniioni();

   public function geo_vdm_province($cod_provincia);
   public function geo_vdm_regioni($cod_regione);

   public function annunci_per_regione();
   public function annunci_salario_italia();

   public function salario($regione);

   public function confronto_annunci();
   public function confronto_esperienza();
   public function confronto_contratto();
   public function confronto_salario();
}