<?php

namespace App\Repository\Eloquent;

use App\Models\Import;
use App\Repository\ImportRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator ;
use Illuminate\Support\Facades\Schema;
use DB;

class ImportRepository extends BaseRepository implements ImportRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    public function __construct(Import $model)
    {
        parent::__construct($model);
    }

    /**
    * @return Collection
    */
    public function all(): LengthAwarePaginator 
    {

        $model = $this->model;                

        return $model->orderBy('created_at', 'DESC')->paginate(20);    
    }

    
    public function createTable($professional_family_slug, $action){
        /**
         * CREATE TABLE mytable(
            general_id        INTEGER  NOT NULL PRIMARY KEY 
            ,year_grab_date    INTEGER  NOT NULL
            ,month_grab_date   INTEGER  NOT NULL
            ,esco_level_4      VARCHAR(82) NOT NULL
            ,detailocc_label   VARCHAR(28) NOT NULL
            ,province          VARCHAR(21)
            ,region            VARCHAR(21)
            ,contract          VARCHAR(23)
            ,macro_sector      VARCHAR(77) NOT NULL
            ,category_sector   VARCHAR(24) NOT NULL
            ,salary            VARCHAR(22)
            ,experience        VARCHAR(18)
            ,companyraw        VARCHAR(129)
            ,escoskill_level_3 VARCHAR(79)
            ,escoskill_level_2 VARCHAR(36)
            ,escoskill_level_0 VARCHAR(9)
            );
         */

        if( !Schema::hasTable($professional_family_slug) ):
            Schema::create($professional_family_slug, function($table)
            {
                $table->integer('general_id');
                $table->integer('year_grab_date');
                $table->integer('month_grab_date');
                $table->string('esco_level_4');
                $table->string('detailocc_label');
                $table->string('province')->nullable();
                $table->string('region')->nullable();
                $table->string('contract')->nullable();
                $table->string('macro_sector');
                $table->string('category_sector');
                $table->string('salary')->nullable();
                $table->string('experience')->nullable();
                $table->string('companyraw')->nullable();
                $table->string('escoskill_level_3')->nullable();
                $table->string('escoskill_level_2')->nullable();
                $table->string('escoskill_level_0')->nullable();

                $table->index('general_id');
                $table->index('detailocc_label');
                $table->index('contract');
                $table->index('salary');
                $table->index('companyraw');
                $table->index('escoskill_level_2');
                $table->index('escoskill_level_0');
                
            });
        elseif( $action == 0 ):
            DB::table($professional_family_slug)->truncate();
        endif;
    }

}