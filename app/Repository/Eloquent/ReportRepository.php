<?php

namespace App\Repository\Eloquent;

use Illuminate\Support\Facades\DB;
use App\Repository\ReportRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ReportRepository extends BaseRepository implements ReportRepositoryInterface
{
    private $professional_family;

    public function __construct(){
        
    }

    /**
     * setProfessionalFamily
     * 
     */
    public function setProfessionalFamily($professional_family)
    {        
        $check = DB::table($this->professional_family)->selectRaw('COUNT(*)')->get();
        if($check):
            $this->professional_family = $professional_family;
        endif;        
    }

    /**
    * @return Collection
    */
    public function all(): Collection
    {                
        return $this->model->all();    
    }

    public function tot_righe()
    {
        $data = DB::table($this->professional_family)
                    ->selectRaw('COUNT(*) as tot_righe')
                    ->first();

        return $data;
    }

    public function ultimi_12_mesi(){
        $data = DB::select("SELECT DATE_FORMAT(CURDATE()-INTERVAL 14 MONTH, '%Y-%m') mese UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 11 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 10 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 9 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 8 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 7 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 6 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 5 MONTH, '%Y-%m') UNION ALL
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 4 MONTH, '%Y-%m') UNION ALL 
                            SELECT DATE_FORMAT(CURDATE()-INTERVAL 3 MONTH, '%Y-%m')");
        
        return $data;
    }
   
    public function andamento(){

        /**
         * SELECT concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) month_grab_date, COUNT(distinct general_id) annunci
            FROM architettura
            GROUP BY concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0'))
            ORDER BY month_grab_date DESC
            LIMIT 12
         */
        
        $data = DB::table($this->professional_family)
                    ->selectRaw('concat(year_grab_date, \'-\', LPAD(month_grab_date, 2, \'0\')) month_grab_date, COUNT(distinct general_id) annunci')
                    ->groupByRaw('concat(year_grab_date, \'-\', LPAD(month_grab_date, 2, \'0\'))')
                    ->orderBy('month_grab_date', 'DESC')                    
                    ->get()
                    ->take(12)
                    ->pluck('annunci', 'month_grab_date')                    
                    ->toArray()
                    ;
        
        return $data;
    }

    public function tot_annunci(){
        $data = DB::select("SELECT COUNT(distinct general_id) as totale FROM `" . $this->professional_family . "`");
        return $data;
    }

    public function tot_annunci_ultimi_12_mesi()
    {        
        $data = DB::select("SELECT COUNT(distinct general_id) as totale 
                              FROM `" . $this->professional_family . "`
                              WHERE concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')");
        return $data;
    }

    public function tot_annunci_ultimi_12_aziende()
    {        
        $data = DB::select("SELECT COUNT(distinct general_id) as totale 
                              FROM `" . $this->professional_family . "`
                              WHERE companyraw IS NOT NULL AND companyraw != '' AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')");
        return $data;
    }


    public function tot_annunci_ultimi_12_esperienza()
    {        
        $data = DB::select("SELECT COUNT(distinct general_id) as totale 
                              FROM `" . $this->professional_family . "`
                              WHERE experience IS NOT NULL AND experience != '' AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')");
        return $data;
    }

    public function tot_annunci_ultimi_12_contratto()
    {        
        $data = DB::select("SELECT COUNT(distinct general_id) as totale 
                              FROM `" . $this->professional_family . "`
                              WHERE contract IS NOT NULL AND contract != '' AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')");
        return $data;
    }

    public function tot_annunci_ultimi_12_salario()
    {        
        $data = DB::select("SELECT COUNT(distinct general_id) as totale 
                              FROM `" . $this->professional_family . "`
                              WHERE salary IS NOT NULL AND salary != '' AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')");
        return $data;
    }

    public function tot_annunci_vdm($vdm){
        $data = DB::select("SELECT COUNT(distinct general_id) as totale
                              FROM `" . $this->professional_family . "` 
                             WHERE detailocc_label = '" . addslashes($vdm) . "'
                               AND escoskill_level_3 IS NOT NULL
                               AND escoskill_level_3 != ''");
        return $data;
    }

    public function professioni()
    {
        /*
        SELECT detailocc_label, COUNT(detailocc_label) tot_voce_di_mercato
        FROM architettura
        GROUP BY detailocc_label
        ORDER BY 2 DESC
        LIMIT 10
        */        

        $data = DB::table($this->professional_family)
                    ->selectRaw('detailocc_label, COUNT(distinct general_id) tot_voce_di_mercato')
                    ->groupBy('detailocc_label')
                    ->orderBy('tot_voce_di_mercato', 'DESC')               
                    ->get()     
                    ->take(10)                    
                    ;                    

        return $data;                    
    }

    public function aziende(){
        /*
        SELECT companyraw , COUNT(distinct general_id) totale
        FROM architettura
        WHERE companyraw IS NOT NULL
        AND companyraw NOT LIKE '%undefined%'
        GROUP BY companyraw
        ORDER BY totale DESC
        LIMIT 10
        */

        $data = DB::table($this->professional_family)
                    ->selectRaw('companyraw , COUNT(distinct general_id) totale')
                    ->whereNotNull('companyraw')
                    ->where('companyraw', '!=', '')
                    ->where('companyraw', 'NOT LIKE', '%undefined%')
                    ->groupBy('companyraw')
                    ->orderBy('totale', 'DESC')               
                    ->get()     
                    ->take(10)                    
                    ;

        return $data;
    }

    public function esperienza(){
        /**
         * SELECT experience, COUNT(DISTINCT general_id) as tot_experience
            FROM architettura
            WHERE experience IS NOT NULL
            AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')
            GROUP BY experience
            ORDER BY totale DESC
         */

        $data = DB::table($this->professional_family)
                    ->selectRaw("experience, COUNT(distinct general_id) tot_experience")                    
                    ->whereNotNull('experience')  
                    ->where('experience', '!=', '')  
                    ->whereRaw("concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')")
                    ->groupBy('experience')
                    ->orderBy('tot_experience', 'DESC')               
                    ->get()                         
                    ;                            

        return $data;
    }

    public function contratto(){
        /**
         * SELECT contract, COUNT(DISTINCT general_id) as tot_contract
            FROM architettura
            WHERE contract IS NOT null
            AND concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m') 
            GROUP BY contract
            ORDER BY tot_contract DESC
            
         */

        $data = DB::table($this->professional_family)
                    ->selectRaw("contract, COUNT(DISTINCT general_id) tot_contract")
                    ->whereNotNull('contract')  
                    ->where('contract', '!=', '')  
                    ->whereRaw("concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')")                  
                    ->groupBy('contract')
                    ->orderBy('tot_contract', 'DESC')                    
                    ->get()
                    ;

        return $data;
    }

    public function elenco_voci_di_mercato(){        
        $data = DB::table($this->professional_family)
                    ->selectRaw('DISTINCT detailocc_label')                                                            
                    ->orderBy('detailocc_label', 'ASC')                                   
                    ->get()                            
                    ->pluck('detailocc_label')
                    ->toArray()              
                    ;

        return $data;
    }

    public function elenco_regioni(){        
        $data = DB::table('regioni')
                    ->selectRaw('DISTINCT code, name')                                                                                
                    ->orderBy('name')
                    ->get()                                                
                    ->pluck('name', 'code')
                    ->toArray()              
                    ;

        return $data;
    }

    public function elenco_province(){
        /**
         * SELECT DISTINCT detailocc_label FROM architettura ORDER BY detailocc_label;
         */

        /** Ho dovuto modificare rispetto alla query, non prende la group by con il case */
        $data = DB::table('province')
                    ->selectRaw('DISTINCT code, name')                                                                                
                    ->get()                            
                    ->pluck('name', 'code')
                    ->toArray()              
                    ;

        return $data;
    }

    public function elenco_competenze(){    
        
        $data = DB::table($this->professional_family)
                    ->selectRaw('DISTINCT escoskill_level_3')       
                    ->whereNotNull('escoskill_level_3')                                                     
                    ->orderBy('escoskill_level_3', 'ASC')                                   
                    ->get()                            
                    ->pluck('escoskill_level_3')
                    ->toArray()              
                    ;

        return $data;
    }

    public function bilancio_competenze()
    {
        
        $data = DB::select("SELECT vdm, 
                                ROUND( ( sum(hard) * 100 ) / ( sum(hard) + sum(soft) ) , 2) hard, 
                                ROUND( ( sum(soft) * 100 ) / ( sum(hard) + sum(soft) ) , 2) soft, 
                                ROUND( ( sum(no_digital) * 100 ) / ( sum(no_digital) + sum(digital) ) , 2) no_digital, 
                                ROUND( ( sum(digital) * 100 ) / ( sum(no_digital) + sum(digital) ) , 2) digital		 
                        FROM (
                        select detailocc_label vdm,
                                    general_id, 		 
                                    sum(case when escoskill_level_0 = 'hard' then 1 ELSE 0 END) hard, 
                                    sum(case when escoskill_level_0 = 'soft' then 1 ELSE 0 END) soft,
                                    sum(case when escoskill_level_2 = 'No digital' then 1 ELSE 0 END) 'no_digital',
                                    sum(case when escoskill_level_2 = 'No digital' then 0 ELSE 1 END) 'digital' 
                        FROM `$this->professional_family`
                        GROUP BY detailocc_label, general_id
                        ORDER BY 1, 2
                        ) a
                        GROUP BY vdm");        
                                
        return $data;                                
    }    

    public function top_competenze($voce_di_mercato)
    {        
        /**
          SELECT a.escoskill_level_3, COUNT(DISTINCT general_id) tot_competenze
          FROM [table]
          WHERE detailocc_label = '[voce di mercato]'
          group by a.escoskill_level_3
         */
        $data = DB::table($this->professional_family . ' as a')
                ->selectRaw('a.escoskill_level_3, COUNT(DISTINCT general_id) tot_competenze')
                ->where('detailocc_label', $voce_di_mercato)                               
                ->where('a.escoskill_level_3', '!=', '')                               
                ->whereNotNull('a.escoskill_level_3')                               
                ->groupByRaw('a.escoskill_level_3')               
                ->orderBy('tot_competenze', 'DESC')                                   
                ->pluck('tot_competenze', 'escoskill_level_3')                          
                ->take(10)
                ->toArray()                
                ;

        return $data;
    }

    public function geo_annunci_province()
    {
        $data = DB::table($this->professional_family . ' as a')
                    ->selectRaw('p.code as prov, COUNT(distinct a.general_id) tot')
                    ->leftJoin('province as p', 'a.province', '=', 'p.name')
                    ->whereNotNull('a.province')                                                   
                    ->groupBy('p.code')               
                    ->orderBy('tot', 'DESC')                                   
                    ->get()      
                    ->pluck('tot', 'prov')                                              
                    ->toArray()                
                    ;
        return $data;
    }

    public function regioniioni()
    {
        /**
         * SELECT r.code, COUNT(distinct a.general_id) tot
            FROM architettura a
            left JOIN regioni r ON a.region = r.name
            WHERE a.region IS NOT null
            GROUP BY r.code
            ORDER BY 2 desc
         */
        $data = DB::table($this->professional_family . ' as a')
                    ->selectRaw('r.code as regione, COUNT(distinct a.general_id) tot')
                    ->leftJoin('regioni as r', 'a.region', '=', 'r.name')
                    ->whereNotNull('a.region')         
                    ->where('a.region', '!=', '')
                    ->groupBy('r.code')               
                    ->orderBy('tot', 'DESC')                                                           
                    ->get()                                           
                    ->pluck('tot', 'regione')                                        
                    ->toArray()                
                    ;
                    
        return $data;
    }

    public function geo_vdm_regioni($cod_regione)
    {        
        $data = DB::table($this->professional_family . ' as a')
                    ->selectRaw('a.detailocc_label vdm , COUNT(distinct a.general_id) as tot')
                    ->leftJoin('regioni as r', 'a.region', '=', 'r.name')
                    ->whereNotNull('a.region')                                                                       
                    ->whereNotNull('a.detailocc_label')   
                    ->where('r.code', $cod_regione)         
                    ->groupBy('a.detailocc_label')               
                    ->orderBy('tot', 'DESC')                                                           
                    ->get()                                           
                    ->pluck('tot', 'vdm')                                        
                    ->toArray()                
                    ;

         return $data;
    }

    public function geo_vdm_province($cod_provincia)
    {
        $data = DB::table($this->professional_family . ' as a')
                    ->selectRaw('a.detailocc_label vdm, COUNT(distinct a.general_id) as tot')
                    ->leftJoin('province as p', 'a.province', '=', 'p.name')
                    ->whereNotNull('a.province')                                                   
                    ->where('p.code', $cod_provincia)                                                   
                    ->groupBy('a.detailocc_label')               
                    ->orderBy('tot', 'DESC')                                                           
                    ->get()                                           
                    ->pluck('tot', 'vdm')                                        
                    ->toArray()                
                    ;                    

         return $data;
    }

    public function annunci_per_regione()
    {
        /**
         * SELECT r.code, COUNT(*) tot
            FROM architettura a
            left JOIN regioni r ON a.region = r.name
            WHERE a.region IS NOT null
            GROUP BY r.code
            ORDER BY 2 DESC
         */

        $data = DB::table($this->professional_family . ' as a')
                ->selectRaw('r.code as region , COUNT(distinct a.general_id) as tot')
                ->leftJoin('regioni as r', 'a.region', '=', 'r.name')
                ->whereNotNull('a.region')  
                ->whereNotNull('a.salary')                                                                       
                ->groupBy('r.code')               
                ->orderBy('tot', 'DESC')                                                           
                ->get()                                           
                ->pluck('tot', 'region')                                        
                ->toArray()                
                ;

        return $data;
    }

    public function annunci_salario_italia()
    {        
        $data = DB::table($this->professional_family . ' as a')
                ->selectRaw('a.salary, COUNT(distinct a.general_id) as tot')                                
                ->whereNotNull('a.salary')
                ->where('a.salary', '!=', '')
                ->groupBy('a.salary')
                ->get()   
                ->pluck('tot', 'salary')                                                        
                ->toArray()                
                ;

        return $data;
    }

    public function salario($regione){        

        if( $regione !== 'IT' ):
            $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw('a.salary, COUNT(distinct a.general_id) tot')
                        ->leftJoin('regioni as r', 'a.region', '=', 'r.name')
                        ->where('r.code', $regione)
                        ->whereNotNull('a.region')                                                                   
                        ->whereNotNull('a.salary')                                                                                           
                        ->groupBy('a.salary')               
                        ->orderBy('a.salary')                                                           
                        ->get()                                           
                        ->pluck('tot', 'salary')                                        
                        ->toArray()                
                        ;
        else:
            $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw('a.salary, COUNT(distinct a.general_id) tot') 
                        ->whereNotNull('a.region')                                                  
                        ->whereNotNull('a.salary')                                                                                           
                        ->groupBy('a.salary')               
                        ->orderBy('a.salary', 'DESC')                                                           
                        ->get()                                           
                        ->pluck('tot', 'salary')                                        
                        ->toArray()                
                        ;
        endif;

        return $data;
    }

    public function confronto_annunci()
    {       
        $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw("CONCAT(year_grab_date, '-', lpad(month_grab_date, 2, '0')) mese, COUNT(DISTINCT general_id) tot")                                                                    
                        ->groupByRaw("CONCAT(year_grab_date, '-', lpad(month_grab_date, 2, '0'))")               
                        ->orderBy('mese', 'DESC')                                                           
                        ->get()                                           
                        ->pluck('tot', 'mese')                                                                
                        ->toArray()                                        
                        ;                                

        return $data;                        
    }

    public function confronto_esperienza()
    {
        /*
        SELECT experience , COUNT(DISTINCT general_id) tot
        FROM architettura a
        WHERE a.detailocc_label = 'architect'
        AND a.experience IS NOT null
        AND CONCAT(year_grab_date, '-', lpad(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE() - INTERVAL 12 MONTH, '%Y-%m') 
        GROUP BY CONCAT(year_grab_date, '-', lpad(month_grab_date, 2, '0')) 
        */

        $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw("experience , COUNT(DISTINCT general_id) tot")                                                                        
                        ->whereRaw("concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')")
                        ->whereNotNull('a.experience')
                        ->where('a.experience', '!=', '')
                        //->groupByRaw("CONCAT(year_grab_date, '-', lpad(month_grab_date, 2, '0')) ")                                       
                        ->groupBy('experience')
                        ->get()                                           
                        ->pluck('tot', 'experience')                                                                
                        ->toArray()                                        
                        ;                                

        return $data;                        
    }

    public function confronto_contratto()
    {        
        $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw("contract , COUNT(DISTINCT general_id) tot")                                                                        
                        ->whereRaw("concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')")
                        ->whereNotNull('a.contract')
                        ->where('a.contract', '!=', '')
                        ->groupBy("contract")                                       
                        ->get()                                           
                        ->pluck('tot', 'contract')                                                                
                        ->toArray()                                        
                        ;                                

        return $data;                        
    }

    public function confronto_salario()
    {        

        $data = DB::table($this->professional_family . ' as a')
                        ->selectRaw("salary , COUNT(DISTINCT general_id) tot")           
                        ->whereRaw("concat(year_grab_date, '-', LPAD(month_grab_date, 2, '0')) >= DATE_FORMAT(CURDATE()-INTERVAL 12 MONTH, '%Y-%m')")                                                             
                        ->whereNotNull('a.salary')
                        ->where('a.salary', '!=', '')
                        ->groupBy("salary")                                       
                        ->get()                                           
                        ->pluck('tot', 'salary')                                                                
                        ->toArray()                                        
                        ;                                

        return $data;                        
    }
}