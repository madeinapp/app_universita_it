<?php

namespace App\Repository\Eloquent;

use App\Models\Role;
use App\Repository\RoleRepositoryInterface;
use Illuminate\Support\Collection;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    /**
        * @return Collection
        */
    public function all(): Collection
    {
        return $this->model->all();    
    }
}