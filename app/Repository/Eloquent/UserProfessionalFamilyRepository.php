<?php

namespace App\Repository\Eloquent;

use App\Models\UserProfessionalFamily;
use App\Repository\UserProfessionalFamilyRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;   

class UserProfessionalFamilyRepository extends BaseRepository implements UserProfessionalFamilyRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param ProfessionalFamily $model
    */
    public function __construct(UserProfessionalFamily $model)
    {
        parent::__construct($model);
    }

    /**
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->model->all();    
    }    

    public function create(array $attributes): Model 
    {        
        return $this->model->create($attributes);
    }

    public function deleteByUser($user_id): bool 
    {        
        return $this->model->where('user_id', $user_id)->delete();        
    }

}