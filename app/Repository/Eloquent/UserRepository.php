<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator ;


class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
    * @return Collection
    */
    public function all($filter_name, $filter_surname): LengthAwarePaginator 
    {

        $model = $this->model;        

        if( $filter_name ):            
            $model = $model->where('name', 'like', '%'.$filter_name.'%');
        endif;

        if( $filter_surname ):
            $model = $model->where('surname', 'like', '%'.$filter_surname.'%');
        endif;

        return $model->paginate(20);    
    }

    /**
    * @return array
    */
    public function professionalFamilies($user_id): array
    {
        $user = $this->model->find($user_id);
        
        $response = array();
        foreach($user->professional_families as $user_professional_family): 
            $response[] = $user_professional_family->id;
        endforeach;

        return $response;
    }    

}