<?php

namespace App\Repository\Eloquent;

use App\Models\ProfessionalFamily;
use App\Repository\ProfessionalFamilyRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;   
use Illuminate\Support\Facades\Schema;

use DB;

class ProfessionalFamilyRepository extends BaseRepository implements ProfessionalFamilyRepositoryInterface
{

    /**
    * UserRepository constructor.
    *
    * @param ProfessionalFamily $model
    */
    public function __construct(ProfessionalFamily $model)
    {
        parent::__construct($model);
    }

    /**
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->model->orderBy('menu_text')->get();    
    }    

    public function create(array $attributes): Model 
    {        
        return $this->model->create($attributes);
    }

    public function findBySlug($slug): Model
    {
        return $this->model->where('slug', $slug)->first();
    }
    
    public function rows($slug)
    {        
        if( Schema::hasTable($slug) ):
            return number_format(DB::table($slug)->get()->count(), 0, ',', '.');        
        else: 
            return 0;
        endif;
    }
}