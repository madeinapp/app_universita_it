<?php
namespace App\Repository;

use Illuminate\Pagination\LengthAwarePaginator ;

interface ImportRepositoryInterface
{
   public function all(): LengthAwarePaginator;   

   public function createTable($professional_family_slug, $action);   
}