<?php
namespace App\Repository;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Type\Integer;

interface ProfessionalFamilyRepositoryInterface
{
   public function all(): Collection;

   public function findBySlug($slug): Model;

   public function rows($slug);
   
}