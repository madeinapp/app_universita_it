<?php
namespace App\Repository;

use Illuminate\Pagination\LengthAwarePaginator ;

interface UserRepositoryInterface
{
   public function all($filter_name, $filer_surname): LengthAwarePaginator;

   public function professionalFamilies($user_id): array;   
}